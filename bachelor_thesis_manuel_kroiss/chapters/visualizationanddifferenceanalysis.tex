%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Visualization and Difference Analysis}
\label{sec:visualizationAndDifferenceAnalysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The following sections describe the architecture of the implemented \ac{AST} visualization and difference analysis tool. The design section \ref{sec:design} discusses different design options and presents the architecture as the result of the design process. The following section models \ref{sec:models} describes the Codemirror component \ref{subsec:codeMirror}, the d3-tree component \ref{subsec:ast}, the Tree model \ref{subsec:treeModel}, and the TreeComparator service \ref{subsec:differenceAnalysis}.

\section{Design}	
\label{sec:design}
The design of the project was one of the most challenging parts at the beginning. The most crucial question was to decide whether to use a frontend framework like Angular \footnote{https://angular.io/} or React \footnote{https://reactjs.org/} or use plain Javascript.\\
The \ac{POC} was a test implementation of a tree visualization using d3js \footnote{https://d3js.org/}, written in Javascript. After some further work, it turned out that there are many dependencies between the different \ac{UI} elements. Thus, it could get challenging to use plain JavaScript only. The winner between Angular and React was Angular because of already knowing the basics of this framework, but it does not matter. The whole project can also be written using React.

The decision to use d3.js was comfortable in the end because this framework provides the freedom that one is not reliable on features of specific tree visualization frameworks. The use of d3.js means that it is more work required to get the desired output, but therefore the created tree visualization component is highly configurable.

The use of CodeMirror \footnote{https://codemirror.net/} was apparent from the beginning because it is the most popular and influential web text editor which provides a massive amount of features and a very well documented API. The same is true for the styling frameworks Bootstrap \footnote{https://getbootstrap.com/} and AdminLTE \footnote{https://adminlte.io}.

\subsection{Architecture}
This section describes the architecture of the tool, which mainly consists of the two components frontend and backend. The backend is responsible for providing a REST interface for transforming a source code file into an \ac{AST}. One can find the detailed description of the backend in the thesis written by Grossauer \cite{Grossauer2018}. The architecture of the frontend shown in figure \ref{fig:architecture} uses the \ac{MVC} pattern and consists of a model component, various view components which the thesis describes later, and a controller that is responsible for loading and processing data into the models as well as handling the business logic of the components.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{figures/architecture}
	\caption{System Architecture}
	\label{fig:architecture}
\end{figure}

\subsubsection{View}
The view holds all relevant components for the visualization. Figure \ref{fig:components} illustrates the different components which this section describes in the following paragraphs.

\paragraph{App} The App component is the root component of the project. This component is responsible for loading the supported languages specified in the \textit{supported\_languages.json} file. The primary goal of the thesis is to create a tool that is independent of the programming language. The problem was that there is a need to give the user the possibility to choose from a set of supported languages. Moreover, there were no other possibilities to get this data. However, the configuration file approach makes it easy to add a new language to the tool.

\paragraph{Index} This component is only the link between the App component and the main components, consisting of the AppHeader component, the Content component, the SettingsComponent, and the AppFooter component. The Angular Router Module needs this link component because the App component needs to specify the \textit{<router-outlet>} as the template so that it is not possible to specify a custom template. The Index component includes all principal components.

\paragraph{AppHeader} The AppHeader only manages to display the site heading and the button to collapse and expand the menu.

\paragraph{Content} This component is the core component of the App. The Content component displays the FileChooser component as well as the CodeMirror component or the d3TreeComponent or both of them depending on the users choice. This component initializes the containers for the other components and handles the communication between its child components.

\paragraph{Settings} The SettingsComponent manages the user needs, \ac{EG}, changing the tree orientation or enable/disable synchronous scrolling of the CodeMirror component.

Next, the upcoming paragraphs characterize the CodeMirror component, the FileChooser component, and the d3TreeComponent.

\paragraph{FileChooser} Developers want to observe changes over multiple file versions. Thus, this component lets the user choose the files that the CodeMirror component and the d3TreeComponent display. The component holds a drop-down menu where the users can select a file that they uploaded in the Filemanager component.

\paragraph{CodeMirror} For many analyzing tasks, developers want to see differences between source code files directly in the code. This component can show the differences that were calculated by the TreeComparator service. The thesis will describe this service later, but for now one can imagine that the service stores a boolean value depending on whether to highlight the corresponding lines or not.

\paragraph{d3Tree} The d3TreeComponent uses d3.js to create a visualization of the abstract syntax tree. This component supports zoom and transform actions, a vertical and a horizontal view of the tree, and collapse and expand of nodes. The user can choose if he wants that the component synchronizes these actions in both views or not. The detailed description of the component follows.

At least, the thesis outlines the absent components Filemanager, FilemanagerContent and the NoFile Component.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{figures/components}
	\caption{Angular Components}
	\label{fig:components}
\end{figure}

\paragraph{NoFile} When the user opens the tool, no files are available to display on the screen. The Content component removes the CodeMirror component and the d3TreeComponent and shows a text where the user can see that he or she must upload files to use the tool to provide better usability. The button directly routes the user to the Filemanager component where file upload is possible.

\paragraph{Filemanager} This component is only the wrapper component for the FileManagerContent component. Its structure is the same as the structure of the Index component. The only difference between the Index component and the Filemanager component is that the Index component replaces the Content component by the FilemanagerContent component.

\paragraph{FilemanagerContent} To upload files to the tool, the user uses this component. The component provides a button that opens a dialog where the user can select files from a file system. Further, users are invited to select a unique name for each file. The FileChooser component uses this name in the drop-down menu. For better usability, the dialog checks that the user has selected a file and provided a unique name. Otherwise, the apply button is not enabled, or the dialog shows different error messages.


\subsection{Controller}
The controller provides the business logic for the components. Each component has its controller, written in TypeScript. The CodeMirror component controller uses the CodeMirror library to show the source code in an editor. The d3TreeComponent controller generates a visualization of the \ac{AST} with the d3.js library.\\
Figure \ref{fig:service} shows the service layer of the architecture. This layer includes a REST service to exchange files and \ac{ASTS} with the backend. The TreeComparator service handles the comparison of two trees. The next section \ref{sec:models} describes the developed difference analysis algorithm.

\begin{figure}[ht]
	\centering
	\includegraphics[width=300px]{figures/service}
	\caption{Service}
	\label{fig:service}
\end{figure}
	
\section{Models}
\label{sec:models}
This section provides detailed information about the most relevant parts of the \ac{AST} visualization tool implementation. The first subsection describes the CodeMirror component, followed by a description of the tree model. Afterward, the thesis provides a precise description of the implementation of the d3Tree component and the TreeComparator.

\subsection{CodeMirror Component}
\label{subsec:codeMirror}
The CodeMirror component creates a new instance of CodeMirror and inserts the source code chosen by the user. A scroll listener listens for scroll actions and informs the Content component to update the scrolling info in the other CodeMirror component. The Content component knows if there is a second instance of the CodeMirror component. If there is a second instance and synchronous scrolling is enabled, the Content component invokes the \textit{scrollTo} method of the CodeMirror component, which handles programmatic scrolling to the provided position.

To support the different programming languages and the corresponding CodeMirror modes, it is necessary to specify the filename of the mode and the mime-type in the configuration file \textit{supported\_languages.json}. Further, one has to add the dynamic import for the specific language in the CodeMirror component, because it is currently not possible to dynamically load a resource with a programmatically constructed path.

\subsection{Tree Model}
\label{subsec:treeModel}
Section \ref{sec:communicationInterface} describes the AstTree as a JSON object provided by the server. It is necessary to transform this JSON object into a TypeScript object to support calculations. The Tree class specifies the hierarchical tree model. The next paragraphs briefly describe the attributes and methods from this model.

\paragraph{Attribute: root} This attribute specifies the root node of the current object.
\paragraph{Attribute: children} The children attribute is an array of Tree models that include all children of the current root node.
\paragraph{Attribute: diff} After processing two trees with the TreeComparator, this field specifies whether the current node is different from the other tree or not.
\paragraph{Attribute: similarHash} To support synchronous expand and collapse for the tree nodes, it is mandatory to know which node is similar to the current one. The similarHash value is set for the second tree and holds the hash value of the node in the first tree. More about this in the following subsection \ref{subsec:ast}.
\paragraph{Attribute: codeMirrorDiff} This attribute is similar to the \textit{diff} attribute, but stores the boolean value for a different node to show up in CodeMirror instead of the \ac{AST} visualization.
\paragraph{Method: markDiff} The \textit{markDiff} method is a helper method for the CodeMirror component. It takes the CodeMirror Component as a parameter an invokes the CodeMirror's \textit{markDiff} function for each node where the \textit{codeMirrorDiff} attribute is true.

\subsection{D3-Tree Component}
\label{subsec:ast}
The D3Tree component uses the d3.js library, especially the provided tree layout, to create an interactive visualization of the \ac{AST}. The d3.js library mainly operates in three states.

The first state is the \textit{enter} state. After the initialization of the layout, the enter state holds all added nodes that are new to the layout data. A specified \textit{enter} function creates new elements for each node. The created elements are a circle element for each node, text elements for the textual description of each node, and SVG-path elements the represent the links between the nodes.\\
The update function handles all updates of the nodes like changing the size triggered by user interaction or collapsing of a node.\\
The third state is the exit state. The specified function of this state handles the process if a node should disappear from the screen.\\
In particular, each node owns the attributes \textit{children} and \textit{\_children}. The \textit{children} element holds elements to display. The \textit{\_children} attribute holds all elements that the visualization should collapse currently. If a collapse or expand operation occurs, the \textit{children} elements move to the \textit{\_children} attribute and the attribute \textit{children} becomes \textit{null}, and the other way round depending on whether the node is currently collapsed or expanded.

Subsection \ref{subsec:treeModel} described the attribute similarHash. The D3Tree component uses this attribute for synchronous collapsing and expanding of the nodes. If a node is collapsed or expanded, the D3Tree component tells the Content component that the node status changed in the current visualization and passes the hashcode of the node to the Content component. The Content component invokes the other D3Tree components \textit{updateNode} method if available and if the synchronous collapse is activated. The \textit{updateNode} method tries to find a node with the same hash code an calls the \textit{collapseExpand} method. For similar nodes, the hash function of the Tree model returns the \textit{similarHash} value.

The synchronous zoom function initializes a \textit{d3.event.transform} object and calculates the $x$ and $y$ coordinates for this event. Afterward, the transform attribute of the tree's draw area takes the new values. If the synchronous zoom is enabled, the function will find the name of the other tree's draw area and uses the same transform object and updates the transform attribute.

\subsection{Difference Analysis Service}
\label{subsec:differenceAnalysis}
This subsection describes the algorithm \textit{calcDiff}. The \textit{calcDiff} algorithm uses the algorithms \textit{splitSimAndDiffNodes} and \textit{markDiffRec}. The last two paragraphs of this section describe these algorithms.

\subsubsection{Algorithm: calcDiff}
Algorithm \ref{alg:calcDiff} (page \pageref{alg:calcDiff}) specifies the calcDiff algorithm that finds the differences between two \textit{Tree} objects that operate as the input of the algorithm. The output of the algorithm is the manipulated tree nodes with \textit{diff} and \textit{codeMirrorDiff} attributes set for different nodes.

At first (line 2 to 5), the function initializes a map where the key takes the hash values of the each node in the tree. The value takes a list of \textit{Tree} elements with the hashcode of these elements equal to the key value.
Furthermore, the algorithm needs a list of child elements of the current node for both trees and a list \textit{diffArray} of \textit{Tree} elements. The algorithm uses the \textit{diffArray} list to store different nodes in both trees found in the analyzing process.

Lines 6 to 15 implement the special case if the node has only one child element - if this case occurs, at first the algorithm checks if the hash codes of the child nodes of each tree differ. If that check evaluates to true, the function sets the \textit{diff} attributes to true and stores the hash value of the child in the first tree into the \textit{simHash} attribute of the second tree's child.

Lines 16 to 21 store the nodes of both trees in the \textit{hashArray} using the hash value of the nodes. As a result, the \textit{hashArray} holds a list with a maximum length of two for each entry in the map.
Next (line 22 to 28), the algorithm checks if there are map entries with a list of length one in the \textit{hashArray}. If a map entry includes a list of length one, the corresponding \textit{Tree} element is different to all nodes in the other tree. Therefore, the algorithm stores the found different nodes in the \textit{differenceArray} and sets the \textit{Tree} element's \textit{diff} attribute to true. Otherwise, that means that the list has a length of two, there are nodes with the same hash values in both trees and therefore are equal, and no further processing is required.

To get to know whether all nodes in the \textit{diffArray} are entirely different or if there are nodes with equal child elements, the algorithm (line 29) specifies the map \textit{simNodesArray} and initializes the map with the result of the \textit{splitSimAndDiffNodes} algorithm. The paper describes this algorithm in the following paragraph.

Lines 30 to 33 check whether the \textit{diffArray} still contains elements. If this is true, the algorithm will call the \textit{markDiffRec} algorithm with the remaining elements and sets the elements'  \textit{codeMirrorDiff} attributes to true.

At least (lines 34 to 37), the \textit{calcDiff} algorithm recursively calls itself to calc the differences of the similar nodes in the \textit{simNodesArray}.

\subsubsection{Algorithm: splitSimAndDiffNodes}
The algorithm \textit{splitSimAndDiffNodes} takes an array of \textit{Tree} elements that contains possible similar nodes and extracts all similar nodes of the array. The algorithm stores the similar nodes in a map that uses an integer key for each pair of similar nodes and stores corresponding nodes in a list of \textit{Tree} elements that act as the value for the key.\\
As a result, the algorithm manipulated the \textit{diffArray} in a sense that it removed all similar nodes from the list. The algorithm returns the map with similar nodes as a list of two \textit{Tree} nodes at each entry. Two nodes are similar if they contain at least one child with the same hash value.

\subsubsection{Algorithm: markDiffRec}
This simple algorithm recursively sets all \textit{diff} attributes of the input node and its children to true.

\section{Repository}
\label{sec:repository}

The GitLab project \footnote{https://gitlab.com/manu.kroiss/BSc\_Stefan\_Grossauer\_Manuel\_Kroiss} includes the source code of the tool. Section \ref{sec:design} described the architecture that one could find in the \textit{frontend} folder of the repository where it is possible to start the server with the command \textit{'ng serve'}.\\
It is necessary to start the backend server additionally to the frontend server. The repository includes the folder \textit{server\_ast\_generation} which is the root of the backend server and one can start it with the command \textit{'gradlew bootRun'}.