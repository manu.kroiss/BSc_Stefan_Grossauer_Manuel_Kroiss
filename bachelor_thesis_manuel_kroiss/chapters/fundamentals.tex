%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Fundamentals}
\label{sec:fundamentals}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This chapter presents the related work for the different parts of the paper. It starts with the papers for the visualization of trees. The second part gives a short introduction to difference analysis in source code evolution, followed by a comparison of already available tools for source code difference analysis.

%=======================================================================
\section{Visualization of Trees}
%=======================================================================
This section first gives an overview of different tree visualization techniques. After that, the paper describes the tool VAST \cite{Almeida-Martinez2009} as the only tool found which implements AST-Visualization.

\subsection{Tree visualization techniques}
\label{subsec:tree_visualization_techniques}
Latorra et al. \cite{Latorra2007} provide an overview of the different use cases of tree visualization. The following paragraphs show an excerpt of the paper focusing on the relevant parts of this thesis, especially the tree type \textit{Parse Trees}, and the different visualization methods.

At first, the paper describes what it means if we speak about a tree. Graph theory defines a tree as a connected graph without cycles. It is further classified as directed, rooted, regular or irreducible.\\
A directed graph is a graph, where the edges have directions. A rooted graph has one individual node that is the designated root whereas all other nodes are child nodes of the root or other child nodes. Regular means that all nodes have the same number of outbound edges. An irreducible tree is a tree which has no nodes with precisely two outbound edges.

\paragraph{Parse Tree}
A parse tree "uses a tree structure to represent productions used to form a given sentence in a formal grammar" \cite{Latorra2007}. The edges represent the use of productions in the grammar. The nodes either represent a terminal symbol (and therefore have no outbound edges) or a production.\\
This sort of tree is rooted and directed and often uses the visualization methods \textit{Focus + Context} and \textit{Semantic Zoom}. The thesis describes these methods later.

The next paragraphs show a collection of visualization methods for graphs, mentioned in the paper \cite{Latorra2007}.

\paragraph{Focus+Context}
Focus + Context has the general idea of displaying the current node or edge in greater detail (the focus) then the other nodes and edges around it (the context).\\
In literature, there are different scaling approaches, which are briefly mentioned but not described further.
\begin{itemize}
	\item Rectilinear Scaling \cite{Munzner2003a}
	\item Degree of Interest Scaling \cite{Budiu2006}
	\item Fisheye views
	\item Hyperbolic trees
	\item "other mathematical models which describe the amount of distortion to be applied to nodes depending on their relation to the focus element" \cite{Latorra2007}
\end{itemize}

\paragraph{Space Optimization}
Especially for small trees, it is essential to be able to fit the trees into a given space without hiding any information. This technique doesn't allow scrolling because it ensures the one can see the whole graph.\\
The best-known tool SOTree \cite{Nguyen2003SOTree} uses subdivisions of available space to allocate space for the sub-trees. The evaluation of this tool shows that there is a small but noticeable increase in usability.\\
Like mentioned before, programmers should use such algorithms primarily on small datasets. If zooming techniques (especially in combination with real-time interaction) are used to visualize trees, these techniques are less useful.

\paragraph{\ac{DOI} Browsing}
In \ac{DOI} Browsing, only a subset of the tree is displayed based on the distance from the current focus node. Like mentioned by Latorra et al. \cite{Latorra2007}, a study shows that this approach has no significant benefits compared to more traditional browsing strategies.

\paragraph{Semantic Zoom}
Semantic Zoom changes the size of nodes based on the current focus instead of typical zooming, which means the enlargement of a portion of the tree by a specific set ratio.\\
The main feature of this approach is that when a focus (node, edge) is selected, the other nodes and edges are "repositioned, resized or removed from the view so that only related nodes are visible" \cite{Latorra2007}.

\paragraph{Comparison of Trees}
Another widespread feature in visualizing trees is the visualization of the comparison of trees. One essential tool is the tool TreeJuxtaposer, created by Munzner et al. \cite{Munzner2003a}.\\
This comparison is described later in \ref{sec:stateOfTheArt_TreeDifferenceAnalysis}.

\subsection{The tool VAST}
Almeida-Martinez et al. \cite{Almeida-Martinez2009} describe the tool VAST (Visualization of Abstract Syntax Trees). It is used in compiler and language processing courses to help students to get a better understanding of their created grammar. The tool allows the generation and visualization of \ac{ASTS}. The primary goal of the tool was to provide a visualization interface which is entirely independent of the parser. It only needs an XML file including the tree information and is designed to handle substantial syntax trees easily.

The complete tool consists of two different modules. The VAST{\tiny API} and the VAST{\tiny VIEW}.

\paragraph{VAST{\tiny API}}
The VAST{\tiny API} is responsible for creating and XML-based information of the syntax tree. The created files act as the data source for VAST{\tiny VIEW}.

\paragraph{VAST{\tiny VIEW}}
The VAST{\tiny VIEW} is the interface which allows the generation and manipulation of the \ac{ST} independent from the parser generator.

The VAST{\tiny API} visualizes the hierarchical structure of the XML file provided as a directed, rooted, irregular tree.\\
This tree has three different nodes. \textit{Terminal Nodes} represent the leaves of the tree. \textit{Non-Terminal Nodes} are either the root or any internal nodes, and \textit{Error Nodes}. \textit{Error Nodes} represent nodes where the parser has recovered from the syntactic parser.

The interface of the tool is mainly designed to allow a natural interaction of the tree as well as always let the user have a picture of the whole tree.\\
The \ac{ST} provides these features by the use of a global and a detailed view. The support for zoom actions, subtree aggregation, and scrolling are liable for the smooth interaction.

\paragraph{Global View}
The global view shows the whole \ac{ST} in an extra window. It highlights the part that is currently visible in the detailed view. Thus, the user always can get a better understanding of the tree although his focus is on specific parts of the tree in the detailed view.
\paragraph{Detailed View}
The detailed view allows the user to inspect specific parts of the tree more detailed. It provides zooming, aggregating the nodes as well as scrolling. The detailed view permanently synchronized all of these actions with the global view.\\
Subtree aggregation, in the means of collapse and expand, allows to only show the exciting parts of the tree by hiding the non-interesting ones. Scrolling allows the user to change the portion of the tree visible in the detailed view. It can either be performed with the scroll bars on the bottom and right of the detailed view or by directly moving the highlighted area in the global view.

\section{Tree Difference Analysis}
\label{sec:stateOfTheArt_TreeDifferenceAnalysis}

The following sections describe different approaches available in the literature to perform source code difference analysis. At first, the paper generally describes what source code difference analysis is and how one can perform it. Afterward, it describes two particular approaches which show specific use-cases of source code difference analysis. At least, the paper describes algorithms and tools for difference analysis and compares them.

\subsection{Source Code Difference Analysis}
Martinez et al. \cite{Martinez2014} describe that source code comparison tools at first create edit scripts. The tools create these edit scripts in a way that they compare two versions of the same source code files and simultaneously store the found differences in the scripts.\\
The goal of the edit scripts is to accurately reflect the actual change that developers performed on a file. Common tools usually only support the detection of adding and deleting lines.

\subsection{Special Use-cases of Difference Analysis}
\label{subsec:special_use_cases_of_difference_analysis}
There are many papers which describe how one can use source code difference analysis to learn from the evolution of source code. Telea et al. \cite{Telea2008}, Chevalier et al. \cite{Chevalier2007a}, and Voinea et al. \cite{Voinea2005} published papers that deal with such analysis. All the tools described in these papers take a look at the whole history of source code files and not only analyze the differences between two files. These tools have some nice features like finding stable software releases or analyzing the software architecture evolution.

Another famous use case of source code difference analysis is plagiarism detection. Cui et al. \cite{Cui2010} for example describes a "Code Comparison System based on Abstract Syntax Trees." The paper \cite{Cui2010} outlines three kinds of code plagiarism detection: text-based, token-based, and syntax structure-based plagiarism detection.
\paragraph{Text-based}
plagiarism detection does not enough meet the requirements of code plagiarism detection. It can only detect copy-and-paste operation, eventually with a little modification like changing the name of methods or variables, or reordering the sequence of statements.
\paragraph{Token-based}
This approach takes into account the syntax structure of the source code and is mainly used to detect code clone as well as plagiarism.
\paragraph{Syntax Structure-based}
This method in its best-known implementation uses \ac{ASTS} to detect code clone. These methods can for example additionally deal with breaking the sequences of function parameters, switch-case statements or function definition statements. The method uses hash values of the \ac{AST} nodes to perform the difference calculation.

\subsection{Algorithms and Tools}
Every developer nearly daily uses diff tools to compare two versions of source code files. The best-known tool is the \textit{Unix Diff Tool} that performs the Meyers algorithm \cite{Myers1986}. This algorithm takes two files, A and B, as input and finds the \ac{SES}, which is equivalent to finding the \ac{LCS}. Therefore, this algorithm creates an edit script \ac{SES} that only contains the commands \textit{deletions from file A} and \textit{insertions into file B}.

The tool \textit{Gumtree}, introduced by Martinez et al. \cite{Martinez2014} implements another source code differencing algorithm. This algorithm, differently to the Meyers algorithm, does not work on finding the \ac{LCS} only. The algorithm implemented in Gumtree first produces an \ac{AST} that is later used for calculating differences on the \ac{AST} level.\\
This approach has significant advantages such as finding move actions. Another example is that the tool can also get to know that a function was added and not only that 14 lines very added somewhere. To dive a little deeper into the details, one has to mention that the tool does not only use \ac{ASTS} but uses fine-grained \ac{ASTS}. This type of an \ac{AST} allows a better understanding of what is going on when source code files are changed.\\
"For instance the return "Foo!"; statement, can be encoded with a single node of type Statement and value return "Foo!" , or with two nodes [...]. If this statement is changed to return "Foo!" + i; , only the fine-grained representation enables to see that the InfixExpression:+ andSimpleName:i nodes are added." \cite{Martinez2014}

The paper now describes an entirely other, straightforward difference algorithm approach to get back to the plagiarism detection tool by Cui et al. \cite{Cui2010} like mentioned before.\\
The tool calculates the syntax tree's hash values for each node of the tree. The calculation of the hash values incorporates the type of the node and the hash value of the subtrees. Then it compares the hash values in the two \ac{ASTS} generated from the source code files and therefore can reduce the difficulty of comparison and still has the advantage that the information of the syntax tree is not lost.\\
This approach is much more straightforward than \ac{EG} the algorithm in the tool Gumtree \cite{Martinez2014}. So this paper makes use of such an algorithm. Therefore the next paragraph describes this hash algorithm more detailed.\\
The main steps, executed on the two different trees, are as follows:
\begin{enumerate}
	\item traverse the syntax tree and calculate the hash value
	\item classify each sub-tree by its child node number - store the information in an array of linked lists
	\item compare the same child nodes (with the same child node number) in the syntax trees
	\item record the sub-trees with according hash values and their source code position
	\item highlight identical parts in the source code files
\end{enumerate}

\newpage
\subsection{Difference Algorithm Comparison}
In this section, the thesis shows a comparison of known difference algorithms in table \ref{table:differenceAlgorithmComparison} (page \pageref{table:differenceAlgorithmComparison}). The first column shows the algorithm name if the authors of the papers have assigned one. The level column lists the level on which the difference algorithm is applied. Columns three to six show a \textit{+} if the algorithm supports the operation (add, delete, move, update) or a \textit{-} otherwise.

One can see that all of the differencing algorithms can only operate on specific programming languages, except the \textit{Unix Diff} by \cite{Myers1986}. The \textit{Unix Diff} tool operates on the plain text level only, so that it is easier to support multiple languages. Thereby, it only supports add- and delete-operations.

The tools JDiff \cite{Apiwattanapong2007}, GumTree \cite{Martinez2014}, and the tool by Neamtiu et al. \cite{Neamtiu2005} all operate on the \ac{ST} level. GumTree and the tool by Neamtiu et al. use an \ac{AST} and JDiff uses a Hammock. A Hammock, briefly explained, is an isolated subgraph H of the graph G with precisely one entry node an one exit node. All of these algorithms facilitate the essential edit operations that can occur while source code files changes. However, all of these tools only support specific programming languages.

The last tool introduced by Munzner et al. \cite{Munzner2003a}, called TreeJuxtaposer, is a little different to the other tools, but worth to mention. The tool is used by biologists to perform tree difference analysis on their particular trees. The aim of this tool is not to find a perfect match, but to introduce a similarity score which matches the nodes that are most similar. Such tools are not suitable for source code differencing analysis, but one can imagine that this approach can be useful in, \ac{EG}, learning from the evolution of source code like mentioned in \ref{subsec:special_use_cases_of_difference_analysis} (page \pageref{subsec:special_use_cases_of_difference_analysis}). The tool TreeJuxtaposer also introduces an efficient implementation of tree visualization. It uses the Focus+Context approach described in \ref{subsec:tree_visualization_techniques} (page \pageref{subsec:tree_visualization_techniques}).

\begin{table}
    \resizebox{\textwidth}{!}{\begin{tabular}{l|l|c|c|c|c|c|l}%
    \bfseries Algorithm name & \bfseries Level & \bfseries Add & \bfseries Delete & \bfseries Move & \bfseries Update & \bfseries Reference & \bfseries Comment % specify table head
    \csvreader[head to column names]{tables/differences_in_source_code_comparison.csv}{}% use head of csv as column names
    {\\\hline\Name\ & \Level & \Add & \Delete & \Move & \Update & \Reference & \Comment}% specify your coloumns here
    \end{tabular}}
\caption{Difference Algorithm Comparison}
\label{table:differenceAlgorithmComparison}
\end{table}