%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction}
\label{sec:introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%=======================================================================
\section{Problem Description}
%=======================================================================
Paper \cite{Hunt1976} describes a tool for textual comparison of source code files. Today, many tools use textual comparison to compare different versions of source code files. 
The mechanisms use a textual comparison, which is possible for all sorts of text files. Source code files are one specific type of text files, so one can use such tools to compare different source code files.

However, a character based comparison operates on the syntactical level only. The syntactical level comparison means that the algorithms used by the tools can only detect simple changes like added or deleted lines. A developer needs much more information if he looks at the result of two compared source code files. The syntactic level is not enough. There is a need for semantic comparison as well.\\
The semantic comparison does not operate directly on the input file but uses \ac{EG} \ac{ASTS} to perform a "better" comparison, which can, \ac{EG}, detect move operations as well as the renaming of variables.

Most of the tools only create an edit script, which contains all relevant information that is needed to convert one file to the other. These scripts are not easy to understand for a developer. Therefore, it is essential to visualize the differences between two source code files. Available tools create visualizations of the differences directly on the files (the source code) on the one hand \cite{Martinez2014} or try to visualize the differences on the \ac{AST} level on the other hand \cite{Munzner2003a}.

There are a lot of papers available which discuss plagiarism detection. Plagiarism detection has the goal to detect similarities in different files. People created many tools for plagiarism detection, and one can modify these approaches to detect differences instead of similarities, which is more or less the same.

Syntactic tools can operate on all files as mentioned earlier. The problem is that syntactic tools cannot provide enough information for the developers.\\
Semantic tools have the opportunity to provide enough information for the developer. The problem here is that they can only operate on a specific programming language.\\

%=======================================================================
\section{Motivation}
%=======================================================================
This paper has the aim to create a visualization tool for source code differencing as well as \ac{AST} differencing to give the user the possibility to choose whether to observe the differences in the source code, on the \ac{AST} level or both to get a better understanding of the changes between files.\\
The tool should be available for a various amount of programming languages, and it should be easy to add new programming languages. Further, the tool should be independent of the implementation of the backend. A communication interface specifies what information is needed to perform the visualization.

A source code difference analysis should have an attractive user interface that is easy to use in the browser without the need of interaction with a terminal. Furthermore, it should allow a smooth interaction with \ac{AST} and source code difference visualization and the user should be able to decide what to see.

The goal is to analyze two different versions of source code from one specific programming language. For example, there is no analyzation of the differences between a specific Java and C file.
%=======================================================================
\section{Expected Results}
%=======================================================================
The thesis creates a tool for client-side visualization of \ac{ASTS} and source code as well
as the visualization of differences between them, based on the hashcode of the nodes (including
their children). The tool will detect and show differences as soon as the hash codes of nodes differ, but corresponding child nodes are detected. It is not the goal to implement higher level algorithms to analyze and show differences like add, remove, delete or move.

Grossauer \cite{Grossauer2018} describes the backend that is responsible for the generation of the AST. This backend provides a REST interface that allows post requests to send source code files and the information of the programming language of the source code file and returns an \ac{AST} that contains all relevant information for comparing. The tool described in this paper uses the backend provided by \cite{Grossauer2018}.

The result is an HTML5 website for desktop-only usage which provides the possibility to upload multiple (different) versions of source code files. The user provides the information about the files' programming language. The goal is to support every programming language. New programming languages are supported nearly automatically, only the alteration of one file is necessary as long as there is support for this language by the tool CodeMirror\footnote{https://codemirror.net/} and the backend \cite{Grossauer2018}.

The frontend sends the uploaded files to the backend. After processing, the backend returns the generated ASTs. The visualization tool then visualizes the differences between the files in two different ways. First, the source code can be shown as text using CodeMirror. Second, there is a tree view of the ASTs and there differences using d3.js\footnote{https://d3js.org/}. The client uses Angular 6 \footnote{https://angular.io/} for the implementation of the client-side logic. For the styling of the page content, the frontend uses Bootstrap4\footnote{https://v4-alpha.getbootstrap.com/} and CSS3 in addition to the framework AdminLTE\footnote{https://adminlte.io/themes/AdminLTE/index2.html} for additional styling and placement of menus, buttons, header and so on.

The next paragraphs show the features of the tool. First, there is a description of the page layout. After that, there is a description of the code view as textual representation followed by the description of the functionality of the AST view.

\subsubsection{The layout of the AST visualization tool}
The AST visualization tool is mainly divided up into a header section, a body section, and an aside section.\\
The header section holds the page title as well as the possibility to fold and unfold the menu.\\
The aside section provides a menu with options for different layouts of the body section which the user can choose. The available options are:
\begin{itemize}
\item Selection of the source files' programming language
\item Manage file versions
\item Choose between vertical and horizontal tree view
\item Activate/Deactivate synchronous zoom of the trees
\item switch between different layouts of the body section (only source code, only AST, ...)
\item more options see \ref{subsub:codemirror}
\end{itemize}
The interface divides the body section into different parts. The top part provides the possibility to choose between multiple source code files to display.\\
The bottom part of the body section shows whatever the user has chosen in the menu:
\begin{itemize}
	\item Version 1
	\begin{itemize}
		\item vertical split: two different source code versions (files)
		\item horizontal split: code view (CodeMirror) and \ac{AST} view
	\end{itemize}
	\item Version 2: two different source code versions (files) in code view
	\item Version 3: two different source code versions (files) as \ac{ASTS}
	\item Version 4: one file in code view
	\item Version 5: one file in \ac{AST} view
\end{itemize}

\subsubsection{Code View (CodeMirror)}
\label{subsub:codemirror}
The code view is done using the tool CodeMirror\footnote{https://codemirror.net/} and will provide the following features as long as there is a support for the chosen programming language in CodeMirror:
\begin{itemize}
	\item line numbers
	\item syntax-highlighting
	\item highlighting of differences between source code versions
	\item synchronous scrolling (this feature can be enabled/disabled in the menu)
\end{itemize}

\subsection{AST view}
The AST view is done using the tool d3.js\footnote{https://d3js.org/}. It will provide these features:
\begin{itemize}
	\item highlighting of differences between source code versions
	\item vertical and horizontal view of the trees
	\item possibility to collapse/expand nodes of the tree (including their children) synchronous in both trees if there are the same or similar nodes (same hash value of root or children) in the different trees
	\item zoom and move (synchronous in both tree views) - this feature can be activated/deactivated in the menu
\end{itemize}

\subsection{Common Interface}
The thesis uses the Swagger-Editor\footnote{https://swagger.io/swagger-editor/} for the specification of the API. The requests sent to the backend are HTTP post requests containing a source code file in plain text including the used programming language. The response to each request is a JSON object containing the generated AST. This AST includes a mapping of each node to the corresponding lines in the source code file.

%=======================================================================
\section{Paper Structure}
%=======================================================================
This paragraph explains the structure of the paper which mainly provides six parts. The next part provides an overview of the current State-of-the-Art in source code difference analysis and \ac{AST} visualization.\\
The following parts cover
\begin{itemize}
	\item the description of the communication interface between frontend and backend
	\item the description of the implementation of the tool, including a detailed explanation of the implemented difference analysis algorithm
	\item the evaluation of the tool
	\item a summary and
	\item future research challenges.
\end{itemize}