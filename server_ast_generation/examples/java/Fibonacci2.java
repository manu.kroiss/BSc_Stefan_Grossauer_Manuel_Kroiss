public class Fibonacci {
    public int fibonacci(int n) {
        if (n < 3) {
            return 5;
        }
        return fibonacci(n -1) + fibonacci(n - 2);
    }
}
