/**
 * NOTE: This class is auto generated by the swagger code generator program (2.3.1).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package api;

import model.AstNode;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-19T08:37:44.108Z")

@Api(value = "ast", description = "the ast API")
public interface AstApi {

    @ApiOperation(value = "All about the summary", nickname = "ast", notes = "Straight forward", response = AstNode.class, tags={ "ast", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = AstNode.class),
        @ApiResponse(code = 400, message = "Something is wrong with the request"),
        @ApiResponse(code = 500, message = "Error with the grammar or the parser generation") })
    @RequestMapping(value = "/ast",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    ResponseEntity<AstNode> ast(
            @ApiParam(value = "The programming language used for the source code", required=true)
            @RequestParam(value="programming_language", required=true)
                    String programmingLanguage,
            @ApiParam(value = "file detail")
            @Valid
            @RequestPart("file")
                    MultipartFile source_code_file);

}
