package api;

import io.swagger.annotations.ApiParam;
import model.AstNode;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import service.java.lexer.JavaLexer;
import service.java.listener.JavaParserBaseListener;
import service.java.listener.impl.JavaParserListener;
import service.java.parser.JavaParser;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;

@Controller
public class AstApiController implements AstApi {
    private final HttpServletRequest request;

    @Autowired
    public AstApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<AstNode> ast(
            @ApiParam(value = "The programming language used for the source code", required = true)
            @RequestParam(value = "programming_language")
                    String programmingLanguage,
            @ApiParam(value = "The source code file", required = true)
            @RequestParam(value = "source_code_file")
            @Valid
                    MultipartFile sourceCodeFile) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json") && programmingLanguage.toLowerCase().equals("java")) {
            AstNode ast = getAst(sourceCodeFile);
            ResponseEntity<AstNode> ret = new ResponseEntity<>(ast, getHeaders(), HttpStatus.OK);
            return ret;
        }

        return new ResponseEntity<AstNode>(HttpStatus.NOT_IMPLEMENTED);
    }

    private AstNode getAst(MultipartFile sourceCodeFile) {
        try {
            CharStream charStream = CharStreams.fromStream(sourceCodeFile.getInputStream());
            JavaLexer javaLexer = new JavaLexer(charStream);
            CommonTokenStream javaCommonTokenStream = new CommonTokenStream(javaLexer);
            JavaParser javaParser = new JavaParser(javaCommonTokenStream);

            ParseTree tree = javaParser.compilationUnit();
            JavaParserBaseListener javaListener = new JavaParserListener();
            ParseTreeWalker.DEFAULT.walk(javaListener, tree);
            return ((JavaParserListener) javaListener).getAst();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccessControlAllowOrigin("*");
        headers.setAccessControlAllowMethods(Arrays.asList(HttpMethod.POST));
        headers.setAccessControlAllowHeaders(Arrays.asList(HttpHeaders.CONTENT_TYPE));
        return headers;
    }
}
