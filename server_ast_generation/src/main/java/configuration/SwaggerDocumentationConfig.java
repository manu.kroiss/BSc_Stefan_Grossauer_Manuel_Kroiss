package configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SuppressWarnings("ALL")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-19T08:37:44.108Z")

@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Abstract Syntax Tree Generation")
            .description("This is a server for generating Abstract Syntax Trees (ASTs) from a given source code file. You can find out more about it at [https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss](   https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss).")
            .license("GNU GPLv3")
            .licenseUrl("https://www.gnu.org/licenses/gpl-3.0.en.html")
            .termsOfServiceUrl("")
            .version("0.1.0")
            .contact(new Contact("","", "e1525664@student.tuwien.ac.at"))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("api"))
                    .build()
                .directModelSubstitute(org.threeten.bp.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.threeten.bp.OffsetDateTime.class, java.util.Date.class)
                .apiInfo(apiInfo());
    }

}
