package service.java.listener.impl;

import model.AstNode;
import org.antlr.v4.runtime.ParserRuleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.java.listener.JavaParserBaseListener;
import service.java.parser.JavaParser;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.LinkedList;

public class JavaParserListener extends JavaParserBaseListener {

    private static final Logger log = LoggerFactory.getLogger(JavaParserListener.class);

    private AstNode ast;
    private Deque<AstNode> parentNodesStack = new LinkedList<>();

    public JavaParserListener() {
        this.ast = new AstNode().type(AstNode.TypeEnum.ROOT);
    }

    @Override
    public void enterClassDeclaration(JavaParser.ClassDeclarationContext ctx) {
        log.debug("Java -- enterClassDeclaration");
        pushToParentNodesStack(AstNode.TypeEnum.CLASS, ctx, ctx.IDENTIFIER().getText());
    }

    @Override
    public void exitClassDeclaration(JavaParser.ClassDeclarationContext ctx) {
        log.debug("Java -- exitClassDeclaration");
        ast.addChildrenItem(parentNodesStack.removeLast());
    }

    @Override
    public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        log.debug("Java -- enterFunctionDeclaration -- name: " + ctx.IDENTIFIER().getText());
        pushToParentNodesStack(AstNode.TypeEnum.FUNCTION_DECLARATION, ctx, ctx.IDENTIFIER().getText());
    }

    @Override
    public void exitMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
        log.debug("Java -- exitFunctionDeclaration -- name: " + ctx.IDENTIFIER().getText());
        addCurrentNodeToParent();
    }

    @Override
    public void enterVariableDeclaratorId(JavaParser.VariableDeclaratorIdContext ctx) {
        AstNode parent = parentNodesStack.getLast();
        String name = ctx.IDENTIFIER().getText();
        switch (parent.getType()) {
            case PARAMETERS:
                log.debug("Java -- enterParameterDeclaration -- name: " + name);
                addChildrenItemToParent(parent, AstNode.TypeEnum.PARAMETER, ctx, name);
                break;
            default:
                log.debug("Java -- enterVariableDeclaration -- name: " + name);
        }
    }

    @Override
    public void enterFormalParameterList(JavaParser.FormalParameterListContext ctx) {
        log.debug("Java -- enterParameters");
        pushToParentNodesStack(AstNode.TypeEnum.PARAMETERS, ctx, null);
    }

    @Override
    public void exitFormalParameterList(JavaParser.FormalParameterListContext ctx) {
        log.debug("Java -- exitParameters");
        addCurrentNodeToParent();
    }

    @Override
    public void enterStatement(JavaParser.StatementContext ctx) {
        AstNode parent = parentNodesStack.getLast();
        if (parent.getType().equals(AstNode.TypeEnum.IF)) {
            log.debug("Java -- enterThenStatements");
            pushToParentNodesStack(AstNode.TypeEnum.THEN, ctx, null);
        } else if (ctx.IF() != null) {
            log.debug("Java -- enterIfStatement");
            pushToParentNodesStack(AstNode.TypeEnum.IF, ctx, null);
        } else if (ctx.RETURN() != null) {
            log.debug("Java -- enterReturnStatement");
            pushToParentNodesStack(AstNode.TypeEnum.RETURN, ctx, null);
        } else {
            log.debug("Java -- enterStatement");
            addChildrenItemToParent(parent, AstNode.TypeEnum.STATEMENT, ctx, ctx.getText());
        }
    }

    @Override
    public void exitStatement(JavaParser.StatementContext ctx) {
        if (ctx.IF() != null) {
            log.debug("Java -- exitIfStatement");
        } else {
            AstNode parent = parentNodesStack.getLast();
            switch (parent.getType()) {
                case THEN:
                    log.debug("Java -- exitThenStatements");
                    break;
                case RETURN:
                    log.debug("Java -- exitReturnStatement");
                    break;
                default:
                    log.debug("Java -- exitStatement");
                    return;
            }
        }
        addCurrentNodeToParent();
    }

    @Override
    public void enterExpression(JavaParser.ExpressionContext ctx) {
        AstNode parent = parentNodesStack.getLast();
        switch (parent.getType()) {
            case IF:
                log.debug("Java -- enterIfCondition");
                pushToParentNodesStack(AstNode.TypeEnum.CONDITION, ctx, null);
                break;
            case PARAMETERS:
                log.debug("Java -- enterFunctionCallParameter");
                pushToParentNodesStack(AstNode.TypeEnum.PARAMETER, ctx, null);
            default:
                //currently do nothing
        }
        if (ctx.bop != null) {
            enterBinaryExpression(ctx);
        }
    }

    @Override
    public void exitExpression(JavaParser.ExpressionContext ctx) {
        if (ctx.bop != null) {
            exitBinaryExpression(ctx);
        }
        AstNode node = parentNodesStack.getLast();
        switch (node.getType()) {
            case CONDITION:
                log.debug("Java -- exitIfCondition");
                addCurrentNodeToParent();
                break;
            case PARAMETER:
                log.debug("Java -- exitFunctionCallParameter");
                addCurrentNodeToParent();
            default:
                //currently do nothing
        }
    }

    private void enterBinaryExpression(JavaParser.ExpressionContext ctx) {
        log.debug("Java -- enterBinaryExpression -- operator: " + ctx.bop.getText());
        parentNodesStack.addLast(new AstNode()
                .type(AstNode.TypeEnum.BINARY_EXPRESSION)
                .startLine(ctx.start.getLine())
                .endLine(ctx.stop.getLine())
                .startChar(ctx.start.getCharPositionInLine())
                .endChar(ctx.stop.getCharPositionInLine())
                .operator(ctx.bop.getText())
        );
    }

    private void exitBinaryExpression(JavaParser.ExpressionContext ctx) {
        log.debug("Java -- exitBinaryExpression -- operator: " + ctx.bop.getText());
        addCurrentNodeToParent();
    }

    @Override
    public void enterPrimary(JavaParser.PrimaryContext ctx) {
        if (ctx.IDENTIFIER() != null) {
            String name = ctx.IDENTIFIER().getText();
            log.debug("Java -- enterVariableLiteral -- name: " + name);
            pushToParentNodesStack(AstNode.TypeEnum.VARIABLE, ctx, name);
        }
    }

    @Override
    public void exitPrimary(JavaParser.PrimaryContext ctx) {
        if (ctx.IDENTIFIER() != null) {
            String name = ctx.IDENTIFIER().getText();
            log.debug("Java -- exitVariableLiteral -- name: " + name);
            addCurrentNodeToParent();
        }
    }

    @Override
    public void enterIntegerLiteral(JavaParser.IntegerLiteralContext ctx) {
        if (ctx.DECIMAL_LITERAL() != null) {
            int value = Integer.parseInt(ctx.DECIMAL_LITERAL().getText());
            log.debug("Java -- enterDecimalLiteral -- value: " + value);
            parentNodesStack.addLast(new AstNode()
                    .type(AstNode.TypeEnum.DECIMAL)
                    .value(BigDecimal.valueOf(value))
                    .startLine(ctx.start.getLine())
                    .endLine(ctx.stop.getLine())
                    .startChar(ctx.start.getCharPositionInLine())
                    .endChar(ctx.stop.getCharPositionInLine())
            );
        }
    }

    @Override
    public void exitIntegerLiteral(JavaParser.IntegerLiteralContext ctx) {
        if (ctx.DECIMAL_LITERAL() != null) {
            int value = Integer.parseInt(ctx.DECIMAL_LITERAL().getText());
            log.debug("Java -- exitDecimalLiteral -- value: " + value);
            addCurrentNodeToParent();
        }
    }

    @Override
    public void enterMethodCall(JavaParser.MethodCallContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        log.debug("Java -- enterFunctionCall -- name: " + name);
        pushToParentNodesStack(AstNode.TypeEnum.FUNCTION_CALL, ctx, name);
    }

    @Override
    public void exitMethodCall(JavaParser.MethodCallContext ctx) {
        String name = ctx.IDENTIFIER().getText();
        log.debug("Java -- exitFunctionCall -- name: " + name);
        addCurrentNodeToParent();
    }

    @Override
    public void enterExpressionList(JavaParser.ExpressionListContext ctx) {
        if (parentNodesStack.getLast().getType().equals(AstNode.TypeEnum.FUNCTION_CALL)) {
            log.debug("Java -- enterFunctionCallParameters");
            pushToParentNodesStack(AstNode.TypeEnum.PARAMETERS, ctx, null);
        }
    }

    @Override
    public void exitExpressionList(JavaParser.ExpressionListContext ctx) {
        if (parentNodesStack.getLast().getType().equals(AstNode.TypeEnum.PARAMETERS)) {
            log.debug("Java -- exitFunctionCallParameters");
            addCurrentNodeToParent();
        }
    }

    private void addCurrentNodeToParent() {
        AstNode node = parentNodesStack.removeLast();
        AstNode parent = parentNodesStack.getLast();
        parent.addChildrenItem(node);
    }

    private void addChildrenItemToParent(
            AstNode parent, AstNode.TypeEnum type, ParserRuleContext ctx, String name) {
        parent.addChildrenItem(createAstNode(type, ctx, name));
    }

    private void pushToParentNodesStack(AstNode.TypeEnum type, ParserRuleContext ctx, String name) {
        parentNodesStack.addLast(createAstNode(type, ctx, name));
    }

    private AstNode createAstNode(AstNode.TypeEnum type, ParserRuleContext ctx, String name) {
        AstNode node = new AstNode()
                .type(type)
                .startLine(ctx.start.getLine())
                .endLine(ctx.stop.getLine())
                .startChar(ctx.start.getCharPositionInLine())
                .endChar(ctx.stop.getCharPositionInLine());

        if (name != null) node.name(name);

        return node;
    }

    public AstNode getAst() {
        return ast;
    }
}
