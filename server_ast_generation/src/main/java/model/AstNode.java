package model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * AstNode
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-19T11:55:27.891Z")

public class AstNode {
    /**
     * The type of the AstNode node
     */
    public enum TypeEnum {
        ROOT("root"),

        CLASS("class"),

        FUNCTION_DECLARATION("function_declaration"),

        PARAMETERS("parameters"),

        PARAMETER("parameter"),

        IF("if"),

        CONDITION("condition"),

        THEN("then"),

        ELSE("else"),

        EXPRESSION("expression"),

        STATEMENT("statement"),

        BINARY_EXPRESSION("binary_expression"),

        VARIABLE("variable"),

        DECIMAL("decimal"),

        FLOAT("float"),

        RETURN("return"),

        FUNCTION_CALL("function_call");

        private String value;

        TypeEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static TypeEnum fromValue(String text) {
            for (TypeEnum b : TypeEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    @JsonProperty("type")
    private TypeEnum type = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("value")
    private BigDecimal value = null;

    @JsonProperty("operator")
    private String operator = null;

    @JsonProperty("startLine")
    private Integer startLine = null;

    @JsonProperty("endLine")
    private Integer endLine = null;

    @JsonProperty("startChar")
    private Integer startChar = null;

    @JsonProperty("endChar")
    private Integer endChar = null;

    @JsonProperty("children")
    @Valid
    private List<AstNode> children = null;

    public AstNode type(TypeEnum type) {
        this.type = type;
        return this;
    }

    /**
     * The type of the AstNode node
     *
     * @return type
     **/
    @ApiModelProperty(value = "The type of the AstNode node")


    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public AstNode name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Used for the name of e.g. a function
     *
     * @return name
     **/
    @ApiModelProperty(value = "Used for the name of e.g. a function")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AstNode value(BigDecimal value) {
        this.value = value;
        return this;
    }

    /**
     * The numeric value of a number -> needs casting
     *
     * @return value
     **/
    @ApiModelProperty(value = "The numeric value of a number -> needs casting")

    @Valid

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public AstNode operator(String operator) {
        this.operator = operator;
        return this;
    }

    /**
     * Used for the operator in a binary expression, e.g. '<'.
     *
     * @return operator
     **/
    @ApiModelProperty(value = "Used for the operator in a binary expression, e.g. '<'.")


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public AstNode startLine(Integer startLine) {
        this.startLine = startLine;
        return this;
    }

    /**
     * Get startLine
     * @return startLine
     **/
    @ApiModelProperty(value = "")


    public Integer getStartLine() {
        return startLine;
    }

    public void setStartLine(Integer startLine) {
        this.startLine = startLine;
    }

    public AstNode endLine(Integer endLine) {
        this.endLine = endLine;
        return this;
    }

    /**
     * Get endLine
     * @return endLine
     **/
    @ApiModelProperty(value = "")


    public Integer getEndLine() {
        return endLine;
    }

    public void setEndLine(Integer endLine) {
        this.endLine = endLine;
    }

    public AstNode startChar(Integer startChar) {
        this.startChar = startChar;
        return this;
    }

    /**
     * Get startChar
     * @return startChar
     **/
    @ApiModelProperty(value = "")


    public Integer getStartChar() {
        return startChar;
    }

    public void setStartChar(Integer startChar) {
        this.startChar = startChar;
    }

    public AstNode endChar(Integer endChar) {
        this.endChar = endChar;
        return this;
    }

    /**
     * Get endChar
     * @return endChar
     **/
    @ApiModelProperty(value = "")


    public Integer getEndChar() {
        return endChar;
    }

    public void setEndChar(Integer endChar) {
        this.endChar = endChar;
    }
    
    public AstNode children(List<AstNode> children) {
        this.children = children;
        return this;
    }

    public AstNode addChildrenItem(AstNode childrenItem) {
        if (this.children == null) {
            this.children = new ArrayList<AstNode>();
        }
        this.children.add(childrenItem);
        return this;
    }

    /**
     * Get children
     *
     * @return children
     **/
    @ApiModelProperty(value = "")

    @Valid

    public List<AstNode> getChildren() {
        return children;
    }

    public void setChildren(List<AstNode> children) {
        this.children = children;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AstNode AstNode = (AstNode) o;
        return Objects.equals(this.type, AstNode.type) &&
                Objects.equals(this.name, AstNode.name) &&
                Objects.equals(this.value, AstNode.value) &&
                Objects.equals(this.children, AstNode.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value, children);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AstNode {\n");

        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    value: ").append(toIndentedString(value)).append("\n");
        sb.append("    children: ").append(toIndentedString(children)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

