let express = require('express');
let fs = require('fs');

let app = express();
let port = 8080;

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get("/treeone", function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    let content = fs.readFileSync("json/treeone.json");
    res.end(content);
});

app.get("/treetwo", function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    let content = fs.readFileSync("json/treetwo.json");
    res.end(content);
});

app.get("/testone", function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    let content = fs.readFileSync("json/testone.json");
    res.end(content);
});

app.get("/testtwo", function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    let content = fs.readFileSync("json/testtwo.json");
    res.end(content);
});

app.get("/", function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end("<h1>Hello, World!</h1>");
});

app.listen(port);
console.log("Server running on port " + port + ".");
