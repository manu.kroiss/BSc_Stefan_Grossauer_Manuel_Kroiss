\documentclass[a4paper,10pt,english]{INSOexpose}
\inputencoding{utf8} % linux, mac
% \inputencoding{latin1} % linux, mac

%\usepackage{pdfpages}

\title{Programming Language Independent Source Code Evolution Analysis Using Abstract Syntax Trees}
\subtitle{Frontend: Difference Analysis and Visualisation\\Backend: Abstract Syntax Tree Generation}
% Bitte setzen falls der Titel zu lang ist
%\shorttitle{Kurztitel}
\author{Manuel Kroiß\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space Stefan Grossauer}
\matrikelnr{01526926\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space 01525664}
\kennzahl{E 033 526\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space E 033 534}
\studium{Wirtschaftsinformatik\space\space\space\space\space\space\space Software and Information Engineering}
\date{\today}
\dokumenttyp{Bachelor Thesis}
\assistent{Johann Grabner}

% Bibliographie file

\IfFileExists{/home/kroissma/Documents/Mendeley_bib_files/BSc_Stefan_Grossauer_Manuel_Kroiss.bib}
	{
		\bibliography{/home/kroissma/Documents/Mendeley_bib_files/BSc_Stefan_Grossauer_Manuel_Kroiss}
	}{
		\bibliography{~/Documents/Mendeley/BSc_Stefan_Grossauer_Manuel_Kroiss}
	}

\begin{document}
\maketitle

%=======================================================================
\section{\langchooser{Problem Description}{Problemstellung}}\label{problemdesc}
%=======================================================================

Today, a textual comparison of source code based on Hunt et al. \cite{Hunt1976} is still the most widely used approach for comparing two different versions of source code. A textual comparison is possible for all sorts of text files, therefore it is also possible for all different programming languages. However, the comparison based on characters is only capable of syntactical code comparison.\\
For comparing source code on the semantics instead of the syntactical level, the comparison has to be based on, for example, Abstract Syntax Trees (ASTs) which contain semantic information, additional to syntactic information. The generation of ASTs depends on information about the used programming language for the source code. To create a source code comparison based on ASTs, that is applicable for all programming languages, the AST generation has to be automated for programming languages new to the source code comparison tool created in this thesis.  A tool, which is generating an abstract format of source code is SrcML\footnote{http://www.srcml.org/}. SrcML generates an abstract XML file from source code which contains semantic information additional to the syntactic information. SrcML currently operates only on a specific set of programming languages (C, C++, C\#, and Java).\\
This thesis creates a tool for the generation of ASTs. The AST generation for programming languages new to the tool is automated and the tool can, therefore, be used for the comparison of source code written in an arbitrary programming language. A web application uses the tool to visualize the differences between multiple source code files.

%=======================================================================
\section{Division of Work}
%=======================================================================

The general topic of this paper, ”Programming Language Independent Source Code Evolution Analysis Using Abstract Syntax Trees”, is divided into two parts.\\
One part, called "Abstract Syntax Tree Generation", covers the server-side and consists of the generation of ASTs (Abstract Syntax Trees) from source code, the second part, called "Difference Analysis and Visualisation", covers the client-side visualisation of ASTs and source code as well as the visualisation of differences between them based on the hashcode of the nodes (including their children). Differences will be shown as soon as the hash code of nodes differs. It is not the goal to implement higher level algorithms to analyse and show differences as add, remove, delete or move.

The first part is done by Stefan Grossauer (01525664). The second part is covered by Manuel Kroiß (01526926).\\
The common interface for the server-client communication is a cooperate work by both.

%=======================================================================
\section{\langchooser{Expected Results/Motivation}{Zielsetzung/Motivation}}
%=======================================================================
This thesis results is a server-client application.\\
The client \ref{Diff_and_Visualisation} visualizes the ASTs and the source codes as well as the differences between multiple source code versions.\\
The server side \ref{AST_Generation} uses a parser, generated from a grammar of a programming language, to generate Abstract Syntax Trees (ASTs). For every file version, an AST is generated. The ASTs are provided by a REST interface.\\
The result of this work can analyse programming-language-independent source code evolution with only one tool. It is not the goal to analyse the source code of two different programming languages with each other. The goal is to analyse two different versions of source code from one specific programming language. For example, there is no analysation of the differences between a specific Java and C file.\\
The architecture of the result is shown in figure \ref{architecture} (page \pageref{architecture}).

\subsection{Difference Analysis and Visualisation} \label{Diff_and_Visualisation}
The result is an HTML5 website for desktop-only usage which provides the possibility to upload multiple (different) versions of source code files. The user provides the information, which programming language is used in the files. The goal is to support every programming language. New programming languages are supported automatically, no alteration of the tool is necessary as long as there is support for this language by the tool CodeMirror\footnote{https://codemirror.net/}.\\
The uploaded files are sent to the backend. After processing, the generated ASTs are returned and the differences between the files are visualised in two different ways. First, the source code can be shown as text using CodeMirror. Second, there is a tree view of the ASTs and there differences using d3.js\footnote{https://d3js.org/}. The styling is done with Bootstrap4\footnote{https://v4-alpha.getbootstrap.com/} and CSS3. The client-side logic is written in JavaScript. The framework AdminLTE\footnote{https://adminlte.io/themes/AdminLTE/index2.html} is used for additional styling and placement of menus, buttons, header and so on.\\
\\
In the appendix (\ref{mockup} and \ref{mockup_menu}), one can see a first mockup of the result. The menu on the left could be collapsed.\\
The next paragraphs show the features which will be provided. First, there is a description of the page layout. After that, there is a description of the code view as textual representation followed by the description of the functionality of the AST view.

\subsubsection{Layout of the AST visualisation tool}
The AST visualisation tool is mainly divided up into a header section, a body section and an aside section.\\
The header section mainly holds the page title as well as the possibility to fold and unfold the menu.\\
The aside section provides a menu with options for different layouts of the body section the user can choose. For example:
\begin{itemize}
\item Selection of the source files' programming language
\item Change between vertical and horizontal tree
\item Activate/Deactivate synchronous scroll/zoom of the trees
\item switch between different layouts of the body section (only source code, only AST, ...)
\end{itemize}
The body section is again divided into different parts. The top part provides the possibility to upload multiple source code files and also allows the user to choose, which source code files should be displayed. Also, scrolling through the files will be possible if there are more then two available.\\
The bottom part of the body section is used to show whatever the user has chosen in the menu. Some examples:
\begin{itemize}
	\item Version 1
	\begin{itemize}
		\item vertical split: two (or more) different source code versions (files)
		\item horizontal split: code view (CodeMirror) and AST view
	\end{itemize}
	\item Version 2: Code view and AST view of one source code file
	\item Version 3: two (or more) different source codes versions (files) in code view
	\item Version 4: two (or more) different source code versions (files) as ASTs
	\item ...
\end{itemize}

\subsubsection{Code View (CodeMirror)}
The code view is done using the tool CodeMirror\footnote{https://codemirror.net/} and will provide the following features as long as there is a support for the chosen programming language in CodeMirror:
\begin{itemize}
	\item line numbers
	\item auto-indent
	\item syntax-highlighting
	\item highlighting of differences between source code versions
	\item synchronous scrolling (this feature can be enabled/disabled in the menu)
\end{itemize}

\subsection{AST view}
The AST view is done using the tool d3.js\footnote{https://d3js.org/}. It will provide these features:
\begin{itemize}
	\item highlighting of differences between source code versions
	\item vertical and horizontal view of the trees
	\item possibility to collapse/expand nodes of the tree (including their children) synchronous in both trees if there are the same nodes (same hash value) in the different trees
	\item zoom and move (synchronous in all tree views) - this feature can be activated/deactivated in the menu
\end{itemize}

\subsection{Common Interface}
The specification of the API is done with Swagger-Editor\footnote{https://swagger.io/swagger-editor/}. The requests sent to the backend are HTTP post requests containing a source code file in plain text including the used programming language. The response to each request is a JSON object containing the generated AST. This AST includes a mapping of each node to the corresponding line in the source code file. Additionally, each node of the tree in the responded JSON object includes one hash value of the node and its child nodes.

\subsection{Abstract Syntax Tree Generation} \label{AST_Generation}
The result of the backend is a RESTful web service written in Java using the Spring Framework\footnote{https://spring.io/guides/gs/rest-service/}. Gradle\footnote{https://gradle.org/} is used as the build tool. The parser is generated with ANTLR\footnote{http://www.antlr.org/}. The tests, done by the JUnit\footnote{http://junit.org/junit5/} Framework, guarantee the correct implementation of the service. For the conversion of Java Objects to JSON and vice versa Jackson\footnote{https://github.com/FasterXML/jackson} is used.

AST for a programming language can be generated, as long as there is an ANTLR 4 grammar in the Github repository for ANTLR 4 grammars\footnote{https://github.com/antlr/grammars-v4}. With this limitation, the supported programming languages are by far more, than the supported programming languages in SrcML. To support a programming language, where the grammar is not in the Github repository, it is sufficient to add the grammar to the repository.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{imgs/architecture}
	\caption{System Architecture}
	\label{architecture}
\end{figure}

%=======================================================================
\section{\langchooser{Methodological Approach}{Methodik}}
%=======================================================================
The methodological approach for the thesis is separated into three steps:
\begin{enumerate}
\item Literature research
\item Design of the common interface
\item Implementation of front- and backend
\end{enumerate}
The source code management and project management is done with Gitlab\footnote{https://gitlab.com/}.

\subsection{Literature Research}
\subsubsection{Difference Analysis and Visualisation}
This part will cover the theoretical comparison of tools for difference analysis with a focus on usability an visualisation.\\
The result of the comparison will show the different tools that are available to visualize differences in two or more source code versions.\\

\subsubsection{Abstract Syntax Tree Generation}
The literature research for the backend focuses on approaches for an generic AST representation for a set of programming languages. State of the art parser generation from programming language grammars and the generation of ASTs using these parsers is also a part of the literature research.

\subsection{Design of the common interface}
This part will cover the creation of the communication interface between server and client. Since there is no state of the art literature for such a communication interface it will be developed from scratch. The expected result will be a specification for a JSON object which will be transferred between server and client.\\

\subsection{Implementation of front- and backend}
\subsubsection{Frontend: Difference Analysis and Visualisation}
The frontend will cover the implementation of a website using JavaScript and other frameworks described in \ref{Diff_and_Visualisation} (page \pageref{Diff_and_Visualisation}).\\
There will be an example of the result by manually creating small source code snippets in different programming languages. Further, there will be a verification using different source code versions from random GitHub repositories. These files, including the changes, will be documented. Then the files will be analysed with the tool and the results will be compared.
\subsubsection{Backend: Abstract Syntax Tree Generation}
The backend will cover the implementation of the AST generation for source code version comparison and the implementation of the REST interface to provide these ASTs.

%=======================================================================
\section{State of the Art}
%=======================================================================
\subsection{Difference Analysis and Visualisation}
For the Visualization part, there will be a study of literature \cite{Latorra2007} and \cite{Almeida-Martinez2009} (more specific in \cite{Almeida-Martinez2009a}).\\
LaTorra \cite{Latorra2007} describes the different trees that can be visualized and the current state of the art visualization methods and as well as interactivity provided by current tools analysed in the paper.\\
Almeida-Martinez et al. \cite{Almeida-Martinez2009} describe the tool VAST which was developed for usage in compiler and language processing courses. This tool does not fit exactly to the topic of this thesis but it uses visualisation of abstract syntax trees to provide a better understanding for students.\\
\\
The part of source code evolution/difference analysis will set up on literature \cite{Cui2010}. The paper \cite{Cui2010} presents a plagiarism detection tool, namely CCS (Code Comparison System), based on abstract syntax trees. This is the simplest approach for source code difference analysis based on comparing the hash values of the different nodes. There are lots of other approaches for more specific source code difference analysis but these specific algorithms are not part of this theses.

\subsection{Abstract Syntax Tree Generation}

As described in Chapter \ref{problemdesc}, srcML \cite{Collard2013} is a tool which generates a generic XML file out of C, C++, C\# and Java source code. The generic XML files are used for the analysis of source code. Querying the source code with XPath and source code transformation with XLST is possible. No information is lost after the transformation from source code to SrcML format and back.
\\
ANTLR \cite{Parr1995}\footnote{http://www.antlr.org/} is a parser generator. The generation of a parser is based on a grammar in EBNF. The parser walking is based on a parse tree. The user can choose to output the parser in either Java or C.
\\
The tool described in the paper \cite{Power2002} modifies the bison parser generator\footnote{https://www.gnu.org/software/bison/} to generate annotated source code in XML format. The XML files contain additional syntactic tags.The tool can be used for arbitrary LALR grammars and is based on the parse tree for a source code file.

%=======================================================================
\section{\langchooser{Table of Contents}{Inhaltsverzeichnis}}
%=======================================================================

\begin{samepage}
  \begin{contentstructure}
  	\item Abstract
  	\item Keywords
    \item Introduction	\estimatedpages{2 pages}
    \item State-of-the-Art \estimatedpages{4 pages}
    \item Communication Interface \estimatedpages{3 pages}
    \begin{contentstructure}
      \item Data Structure Definition
      \item Swagger Editor - API Documentation
    \end{contentstructure}
    \item Implementation \estimatedpages{5 pages}
    \item Evaluation \estimatedpages{2 pages}
    \item Summary \estimatedpages{1 pages}
    \item Future Research Challenges \estimatedpages{1 pages}
    \item Bibliography
    \item Appendix: source code, API documentation, ...
  \end{contentstructure}
\end{samepage}

% Bibliographie
\pagebreak
\printbibliography
\appendix
\newpage
\section{Mockup} \label{mockup}
%\includepdf[pages=-,angle=90,scale=0.9]{imgs/mockup.pdf}
\includegraphics[angle=90,scale=0.35]{imgs/mockup.pdf}
\section{Mockup Menu} \label{mockup_menu}
\includegraphics[angle=90,scale=0.35]{imgs/mockup_menu.pdf}
\end{document}
