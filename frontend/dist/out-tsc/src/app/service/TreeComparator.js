"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TreeComparator = /** @class */ (function () {
    function TreeComparator() {
    }
    /**
     * Extracts the similar nodes from the difference Array and stores the
     * similar nodes in the SimilarNodesArray
     *
     * After execution, the Difference Array only contains completely different nodes.
     *
     * @param {Array<Tree>} DifferenceArray
     * @returns {Array<Array<Tree>>} An array that contains an array (length 2) of similar nodes
     */
    TreeComparator.prototype.splitSimilarAndStandaloneNodes = function (DifferenceArray) {
        var similarNodesArray = [];
        DifferenceArray.forEach(function (d) {
            d.children.forEach(function (c) {
                DifferenceArray.filter(function (x) { return x != d; }).forEach(function (d2) {
                    d2.children.forEach(function (c2) {
                        if (c.hashcode() == c2.hashcode()) {
                            similarNodesArray[similarNodesArray.length] = [];
                            similarNodesArray[similarNodesArray.length - 1].push(d);
                            similarNodesArray[similarNodesArray.length - 1].push(d2);
                            DifferenceArray.splice(DifferenceArray.indexOf(d), 1);
                            DifferenceArray.splice(DifferenceArray.indexOf(d2), 1);
                            return similarNodesArray;
                        }
                    });
                });
            });
        });
        return similarNodesArray;
    };
    TreeComparator.prototype.calcDiff = function (tree1, tree2) {
        var _this = this;
        var hashArray = [];
        var children1 = tree1.children;
        var children2 = tree2.children;
        var differenceArray = [];
        if (children1.length == 1 && children2.length == 1) {
            if (children1[0].hashcode() != children2[0].hashcode()) {
                children1[0].diff = true;
                children2[0].diff = true;
                children2[0].similarHash = children1[0].hashcode();
            }
            this.calcDiff(children1[0], children2[0]);
        }
        else {
            children1.forEach(function (c) {
                if (!hashArray[c.hashcode()]) {
                    hashArray[c.hashcode()] = [];
                }
                hashArray[c.hashcode()].push(c);
            });
            children2.forEach(function (c) {
                if (!hashArray[c.hashcode()]) {
                    hashArray[c.hashcode()] = [];
                }
                hashArray[c.hashcode()].push(c);
            });
            hashArray.forEach(function (el) {
                if (el.length == 1) {
                    el[0].diff = true;
                    differenceArray.push(el[0]);
                }
                // else -> nodes are equal - no further processing required
            });
            var similarNodesArray = this.splitSimilarAndStandaloneNodes(differenceArray);
            // This root and its childs have no similar nodes
            differenceArray.forEach(function (d) {
                _this.markDiffRec(d);
                d.codeMirrorDiff = true;
            });
            // compare all similar nodes
            similarNodesArray.forEach(function (p) {
                p[1].similarHash = p[0].hashcode(); // set fake hashcode for sync collapse of similar nodes
                _this.calcDiff(p[0], p[1]);
            });
        }
    };
    /**
     * Marks the element and all elements children as different
     * @param {Tree} element the root element
     */
    TreeComparator.prototype.markDiffRec = function (element) {
        var _this = this;
        element.diff = true;
        element.children.forEach(function (c) {
            _this.markDiffRec(c);
        });
    };
    return TreeComparator;
}());
exports.TreeComparator = TreeComparator;
//# sourceMappingURL=TreeComparator.js.map