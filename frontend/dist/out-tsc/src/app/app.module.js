"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var appheader_component_1 = require("./components/appheader/appheader.component");
var appfooter_component_1 = require("./components/appfooter/appfooter.component");
var settings_component_1 = require("./components/settings/settings.component");
var content_component_1 = require("./components/content/content.component");
var http_1 = require("@angular/common/http");
var codemirror_component_1 = require("./components/codemirror/codemirror.component");
var d3tree_component_1 = require("./components/d3tree/d3tree.component");
var filechooser_component_1 = require("./components/filechooser/filechooser.component");
var filemanager_component_1 = require("./components/filemanager/filemanager.component");
var app_routing_module_1 = require("./app-routing.module");
var index_component_1 = require("./components/index/index.component");
var filemanagercontent_component_1 = require("./components/filemanagercontent/filemanagercontent.component");
var nofile_component_1 = require("./components/nofile/nofile.component");
var ast_service_1 = require("./service/restService/ast.service");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                appheader_component_1.AppheaderComponent,
                appfooter_component_1.AppfooterComponent,
                settings_component_1.SettingsComponent,
                content_component_1.ContentComponent,
                codemirror_component_1.CodemirrorComponent,
                d3tree_component_1.D3treeComponent,
                filechooser_component_1.FilechooserComponent,
                filemanager_component_1.FilemanagerComponent,
                index_component_1.IndexComponent,
                filemanagercontent_component_1.FilemanagercontentComponent,
                nofile_component_1.NofileComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpClientModule,
                app_routing_module_1.AppRoutingModule
            ],
            providers: [
                settings_component_1.SettingsComponent,
                content_component_1.ContentComponent,
                ast_service_1.AstService
            ],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [
                d3tree_component_1.D3treeComponent,
                codemirror_component_1.CodemirrorComponent,
                content_component_1.ContentComponent,
                nofile_component_1.NofileComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map