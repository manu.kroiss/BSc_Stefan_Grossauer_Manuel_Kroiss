"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SourceFile_1 = require("../../models/SourceFile");
var router_1 = require("@angular/router");
var app_component_1 = require("../../app.component");
var $ = require("jquery");
var http_1 = require("@angular/common/http");
// import {ITree} from '../../models/ITree';
var ast_service_1 = require("../../service/restService/ast.service");
var FilemanagercontentComponent = /** @class */ (function () {
    function FilemanagercontentComponent(router, http, astService) {
        this.router = router;
        this.http = http;
        this.astService = astService;
        this.inputError = false;
        this.idExists = false;
        this.languageError = false;
        this.unknownError = false;
    }
    FilemanagercontentComponent_1 = FilemanagercontentComponent;
    FilemanagercontentComponent.prototype.ngOnInit = function () {
        this.sourceFiles = app_component_1.AppComponent.getSourceFiles();
    };
    FilemanagercontentComponent.prototype.ngAfterViewInit = function () {
        FilemanagercontentComponent_1.addInputLabelChangeListener();
    };
    FilemanagercontentComponent.addInputLabelChangeListener = function () {
        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling, labelVal = label.innerHTML;
            input.addEventListener('change', function (event) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = event.target.value.split('\\').pop();
                if (fileName)
                    label.innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });
    };
    FilemanagercontentComponent.prototype.btnDeleteFile = function (index) {
        var deleted = this.sourceFiles.splice(index, 1);
        if (app_component_1.AppComponent.selectedFile1 === deleted[0]) {
            app_component_1.AppComponent.selectedFile1 = null;
        }
        if (app_component_1.AppComponent.selectedFile2 === deleted[0]) {
            app_component_1.AppComponent.selectedFile2 = null;
        }
    };
    FilemanagercontentComponent.prototype.btnOkClick = function () {
        this.router.navigate(['index']);
    };
    FilemanagercontentComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var eventObj = event;
        var target = eventObj.target;
        var files = target.files;
        this.tmpFile = files[0];
        this.languageError = false;
        this.unknownError = false;
        this.astService.ast(app_component_1.AppComponent.selectedProgrammingLanguage.name.toLowerCase(), files[0])
            .subscribe(function (data) {
            _this.tmpTree = data;
        }, function (error) {
            if (error.status == 501) {
                _this.languageError = true;
            }
            else {
                _this.unknownError = true;
            }
        });
        if (!this.languageError) {
            var reader_1 = new FileReader();
            if (event.target.files && event.target.files.length > 0) {
                var file = event.target.files[0];
                reader_1.readAsText(file, 'UTF-8');
                reader_1.onload = function () {
                    _this.tmpContent = reader_1.result;
                };
            }
        }
    };
    FilemanagercontentComponent.prototype.onSubmit = function (form) {
        if (!form || !form.value || !form.value['identifier'] || this.tmpFile == null) {
            this.inputError = true;
            return;
        }
        else {
            var exist = false;
            for (var i = 0; i < this.sourceFiles.length; i++) {
                if (this.sourceFiles[i].identifier == form.value['identifier']) {
                    this.idExists = true;
                    exist = true;
                    break;
                }
            }
            if (!exist)
                this.idExists = false;
            if (!this.idExists && !this.languageError && !this.unknownError) {
                this.sourceFiles.push(new SourceFile_1.SourceFile(this.tmpFile.name, form.value['identifier'], new Date(this.tmpFile.lastModifiedDate), this.tmpContent, this.tmpTree));
                this.closeModalAndReset(form);
            }
        }
    };
    FilemanagercontentComponent.prototype.closeModalAndReset = function (form) {
        var btnClose = document.getElementById('btnCloseModal');
        btnClose.click();
        form.reset();
        this.tmpFile = null;
        $('#lbl-file-upload').html('Select file');
        this.inputError = false;
        this.idExists = false;
    };
    FilemanagercontentComponent = FilemanagercontentComponent_1 = __decorate([
        core_1.Component({
            selector: 'app-filemanagercontent',
            templateUrl: './filemanagercontent.component.html',
            styleUrls: ['./filemanagercontent.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            http_1.HttpClient,
            ast_service_1.AstService])
    ], FilemanagercontentComponent);
    return FilemanagercontentComponent;
    var FilemanagercontentComponent_1;
}());
exports.FilemanagercontentComponent = FilemanagercontentComponent;
//# sourceMappingURL=filemanagercontent.component.js.map