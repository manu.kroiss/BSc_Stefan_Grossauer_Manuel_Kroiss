"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_component_1 = require("../../app.component");
var DomUtils_1 = require("../../utils/DomUtils");
var router_1 = require("@angular/router");
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(router) {
        this.router = router;
        this.horizontal = true;
        this.supportedLanguages = app_component_1.AppComponent.supportedLanguages;
    }
    SettingsComponent_1 = SettingsComponent;
    SettingsComponent.prototype.liLanguageClick = function (data, index) {
        this.supportedLanguages.forEach(function (lan) {
            lan.selected = false;
        });
        this.supportedLanguages[index].selected = true;
    };
    SettingsComponent.prototype.ngOnInit = function () {
        this.horizontal = app_component_1.AppComponent.horizontalTree;
    };
    SettingsComponent.resetViewOptions = function () {
        app_component_1.AppComponent.viewOptions.codeAndAstDiff = false;
        app_component_1.AppComponent.viewOptions.codeDiff = false;
        app_component_1.AppComponent.viewOptions.astDiff = false;
        app_component_1.AppComponent.viewOptions.codeOnly = false;
        app_component_1.AppComponent.viewOptions.astOnly = false;
    };
    // noinspection JSMethodCanBeStatic
    SettingsComponent.prototype.synchronousZoomOptionClick = function () {
        app_component_1.AppComponent.synchronousZoom = !app_component_1.AppComponent.synchronousZoom;
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncZoom').classList, app_component_1.AppComponent.synchronousZoom);
    };
    SettingsComponent.updateBooleanOption = function (classList, enabled) {
        if (enabled) {
            classList.add('enabled');
        }
        else {
            classList.remove('enabled');
        }
    };
    // noinspection JSMethodCanBeStatic
    SettingsComponent.prototype.synchronousCollapseOptionClick = function () {
        app_component_1.AppComponent.synchronousCollapse = !app_component_1.AppComponent.synchronousCollapse;
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncCollapse').classList, app_component_1.AppComponent.synchronousCollapse);
    };
    // noinspection JSMethodCanBeStatic
    SettingsComponent.prototype.synchronousScrollOptionClick = function () {
        app_component_1.AppComponent.synchronousScroll = !app_component_1.AppComponent.synchronousScroll;
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncScroll').classList, app_component_1.AppComponent.synchronousScroll);
    };
    SettingsComponent.prototype.horizontalTreeOptionClick = function () {
        app_component_1.AppComponent.horizontalTree = true;
        this.horizontal = true;
        this.contentComponent.changeTreeOrientation('horizontal');
    };
    SettingsComponent.prototype.verticalTreeOptionClick = function () {
        app_component_1.AppComponent.horizontalTree = false;
        this.horizontal = false;
        this.contentComponent.changeTreeOrientation('vertical');
    };
    SettingsComponent.setActiveCheckSymbol = function (parent, elementText) {
        parent.innerHTML = '<span>' + elementText + '</span><i class="fa fa-check pull-right"></i>';
    };
    SettingsComponent.prototype.codeAndAstDiffOptionClick = function () {
        SettingsComponent_1.resetViewOptions();
        app_component_1.AppComponent.viewOptions.codeAndAstDiff = true;
        SettingsComponent_1.updateViewOptionSymbol();
        if (this.contentComponent != null)
            this.contentComponent.init();
    };
    SettingsComponent.prototype.codeDiffOptionClick = function () {
        SettingsComponent_1.resetViewOptions();
        app_component_1.AppComponent.viewOptions.codeDiff = true;
        SettingsComponent_1.updateViewOptionSymbol();
        if (this.contentComponent != null)
            this.contentComponent.init();
    };
    SettingsComponent.prototype.astDiffOptionClick = function () {
        SettingsComponent_1.resetViewOptions();
        app_component_1.AppComponent.viewOptions.astDiff = true;
        SettingsComponent_1.updateViewOptionSymbol();
        if (this.contentComponent != null)
            this.contentComponent.init();
    };
    SettingsComponent.prototype.codeOnlyOptionClick = function () {
        SettingsComponent_1.resetViewOptions();
        app_component_1.AppComponent.viewOptions.codeOnly = true;
        SettingsComponent_1.updateViewOptionSymbol();
        if (this.contentComponent != null)
            this.contentComponent.init();
    };
    SettingsComponent.prototype.astOnlyOptionClick = function () {
        SettingsComponent_1.resetViewOptions();
        app_component_1.AppComponent.viewOptions.astOnly = true;
        SettingsComponent_1.updateViewOptionSymbol();
        if (this.contentComponent != null)
            this.contentComponent.init();
    };
    SettingsComponent.updateViewOptionSymbol = function () {
        SettingsComponent_1.resetContentOptionsMenu();
        if (app_component_1.AppComponent.viewOptions.astOnly) {
            SettingsComponent_1.setActiveCheckSymbol(document.getElementById('astOnlyOption'), 'Ast Only');
        }
        if (app_component_1.AppComponent.viewOptions.codeOnly) {
            SettingsComponent_1.setActiveCheckSymbol(document.getElementById('codeOnlyOption'), 'Code Only');
        }
        if (app_component_1.AppComponent.viewOptions.astDiff) {
            SettingsComponent_1.setActiveCheckSymbol(document.getElementById('astDiffOption'), 'AST Diff');
        }
        if (app_component_1.AppComponent.viewOptions.codeAndAstDiff) {
            SettingsComponent_1.setActiveCheckSymbol(document.getElementById('codeAndAstDiffOption'), 'Code + AST Diff');
        }
        if (app_component_1.AppComponent.viewOptions.codeDiff) {
            SettingsComponent_1.setActiveCheckSymbol(document.getElementById('codeDiffOption'), 'Code Diff');
        }
    };
    SettingsComponent.reinitializeSettings = function () {
        SettingsComponent_1.updateViewOptionSymbol();
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncZoom').classList, app_component_1.AppComponent.synchronousZoom);
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncScroll').classList, app_component_1.AppComponent.synchronousScroll);
        SettingsComponent_1.updateBooleanOption(document.getElementById('syncCollapse').classList, app_component_1.AppComponent.synchronousCollapse);
    };
    SettingsComponent.resetContentOptionsMenu = function () {
        var contentOptionMenuChildren = document.getElementById('contentOptionMenu').children;
        SettingsComponent_1.removeIElements(contentOptionMenuChildren);
    };
    SettingsComponent.removeIElements = function (children) {
        for (var i = 0; i < children.length; i++) {
            var child = children[i].children[0];
            DomUtils_1.DomUtils.removeIElementChildren(child);
        }
    };
    SettingsComponent.showFirstCodeMirror = function () {
        return app_component_1.AppComponent.viewOptions.codeAndAstDiff || app_component_1.AppComponent.viewOptions.codeOnly || app_component_1.AppComponent.viewOptions.codeDiff;
    };
    SettingsComponent.showSecondCodeMirror = function () {
        return app_component_1.AppComponent.viewOptions.codeAndAstDiff || app_component_1.AppComponent.viewOptions.codeDiff;
    };
    SettingsComponent.showFirstTree = function () {
        return app_component_1.AppComponent.viewOptions.astOnly || app_component_1.AppComponent.viewOptions.astDiff || app_component_1.AppComponent.viewOptions.codeAndAstDiff;
    };
    SettingsComponent.showSecondTree = function () {
        return app_component_1.AppComponent.viewOptions.codeAndAstDiff || app_component_1.AppComponent.viewOptions.astDiff;
    };
    SettingsComponent.prototype.setContentComponent = function (contentComponent) {
        this.contentComponent = contentComponent;
    };
    SettingsComponent.onlyCode = function () {
        return app_component_1.AppComponent.viewOptions.codeOnly || app_component_1.AppComponent.viewOptions.codeDiff;
    };
    SettingsComponent.onlyAst = function () {
        return app_component_1.AppComponent.viewOptions.astOnly || app_component_1.AppComponent.viewOptions.astDiff;
    };
    SettingsComponent.showDiff = function () {
        return SettingsComponent_1.showSecondTree() || SettingsComponent_1.showSecondCodeMirror();
    };
    SettingsComponent.syncZoomEnabled = function () {
        return document.getElementById('syncZoom').classList.contains('enabled');
    };
    SettingsComponent.syncCollapseEnabled = function () {
        return document.getElementById('syncCollapse').classList.contains('enabled');
    };
    SettingsComponent.syncScrollEnabled = function () {
        return document.getElementById('syncScroll').classList.contains('enabled');
    };
    SettingsComponent.prototype.btnManageFileVersionsClick = function () {
        this.router.navigate(['filemanager']);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], SettingsComponent.prototype, "showSettings", void 0);
    SettingsComponent = SettingsComponent_1 = __decorate([
        core_1.Component({
            selector: 'app-settings',
            templateUrl: './settings.component.html',
            styleUrls: ['./settings.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], SettingsComponent);
    return SettingsComponent;
    var SettingsComponent_1;
}());
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=settings.component.js.map