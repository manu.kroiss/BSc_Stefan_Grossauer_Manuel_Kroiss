"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var codemirror_component_1 = require("../codemirror/codemirror.component");
var d3tree_component_1 = require("../d3tree/d3tree.component");
var settings_component_1 = require("../settings/settings.component");
var $ = require("jquery");
var Constants_1 = require("../../utils/Constants");
var filechooser_component_1 = require("../filechooser/filechooser.component");
var Tree_1 = require("../../models/Tree");
var TreeComparator_1 = require("../../service/TreeComparator");
var app_component_1 = require("../../app.component");
// import {ITree} from '../../models/ITree';
var nofile_component_1 = require("../nofile/nofile.component");
var ContentComponent = /** @class */ (function () {
    function ContentComponent(cfr) {
        this.cfr = cfr;
        this.orientation = 'horizontal';
        this.codeMirrorHeightPercentage = 30; // code mirror height in percent
    }
    ContentComponent.prototype.ngOnInit = function () {
    };
    ContentComponent.prototype.ngAfterViewInit = function () {
        this.filechooserComponent.setContentComponent(this);
        this.init();
    };
    ContentComponent.prototype.init = function () {
        this.filechooserComponent.updateDiff(settings_component_1.SettingsComponent.showDiff());
        this.codeMirrorContainer.clear();
        this.treeContainer.clear();
        d3tree_component_1.D3treeComponent.resetIdCounter();
        settings_component_1.SettingsComponent.reinitializeSettings();
        this.initCodeMirrorComponents();
        this.initTreeComponents();
        this.setComponentSize();
        if (app_component_1.AppComponent.selectedFile1 == null || app_component_1.AppComponent.selectedFile2 == null) {
            var comp = this.cfr.resolveComponentFactory(nofile_component_1.NofileComponent);
            this.treeContainer.createComponent(comp);
        }
        else {
            if (settings_component_1.SettingsComponent.showFirstTree() && !settings_component_1.SettingsComponent.showSecondTree()) {
                var treeOne = void 0;
                if (app_component_1.AppComponent.selectedFile1 != null) {
                    treeOne = app_component_1.AppComponent.selectedFile1.tree;
                }
                if (treeOne != null) {
                    this.leftTreeData = new Tree_1.Tree(treeOne);
                }
            }
            else {
                var treeOne = void 0;
                var treeTwo = void 0;
                if (app_component_1.AppComponent.selectedFile1 != null && app_component_1.AppComponent.selectedFile2 != null) {
                    treeOne = app_component_1.AppComponent.selectedFile1.tree;
                    treeTwo = app_component_1.AppComponent.selectedFile2.tree;
                }
                if (treeOne != null && treeTwo != null) {
                    this.leftTreeData = new Tree_1.Tree(treeOne);
                    this.rightTreeData = new Tree_1.Tree(treeTwo);
                    var comparator = new TreeComparator_1.TreeComparator();
                    comparator.calcDiff(this.leftTreeData, this.rightTreeData);
                }
            }
            this.initCodeMirror();
            this.initTrees();
        }
    };
    ContentComponent.prototype.initTrees = function () {
        if (settings_component_1.SettingsComponent.showFirstTree() && !settings_component_1.SettingsComponent.showSecondTree()) {
            this.leftTree.initTree(this, this.leftTreeData.getAsITreeObject(), this.orientation);
        }
        else {
            this.leftTree.initTree(this, this.leftTreeData.getAsITreeObject(), this.orientation);
            this.rightTree.initTree(this, this.rightTreeData.getAsITreeObject(), this.orientation);
        }
    };
    ContentComponent.prototype.initCodeMirror = function () {
        if (settings_component_1.SettingsComponent.showFirstCodeMirror()) {
            this.leftCodeMirror.initCodeMirror(this, app_component_1.AppComponent.selectedFile1.content);
            if (settings_component_1.SettingsComponent.showSecondCodeMirror()) {
                this.rightCodeMirror.initCodeMirror(this, app_component_1.AppComponent.selectedFile2.content);
                this.setDiff();
            }
        }
    };
    ContentComponent.prototype.updateNode = function (hashcode, caller) {
        if (this.leftTree === caller) {
            this.rightTree.updateNode(hashcode);
        }
        else {
            this.leftTree.updateNode(hashcode);
        }
    };
    ContentComponent.prototype.changeTreeOrientation = function (orientation) {
        this.orientation = orientation;
        this.initTrees();
    };
    ContentComponent.prototype.initTreeComponents = function () {
        if (settings_component_1.SettingsComponent.showFirstTree()) {
            var comp = this.cfr.resolveComponentFactory(d3tree_component_1.D3treeComponent);
            // Create component inside container
            var d3TreeComponent = this.treeContainer.createComponent(comp);
            // see explanations
            this.leftTree = d3TreeComponent.instance;
            this.leftTree.setElementRef(d3TreeComponent);
            this.leftTree.setClass(settings_component_1.SettingsComponent.showSecondTree() ? 'col-sm-6' : 'col-sm-12');
            if (settings_component_1.SettingsComponent.showSecondTree()) {
                d3TreeComponent = this.treeContainer.createComponent(comp);
                // see explanations
                this.rightTree = d3TreeComponent.instance;
                this.rightTree.setElementRef(d3TreeComponent);
                this.rightTree.setClass('col-sm-6');
            }
        }
    };
    ContentComponent.prototype.initCodeMirrorComponents = function () {
        if (settings_component_1.SettingsComponent.showFirstCodeMirror()) {
            var comp = this.cfr.resolveComponentFactory(codemirror_component_1.CodemirrorComponent);
            // Create component inside container
            var codeMirrorComponent = this.codeMirrorContainer.createComponent(comp);
            // see explanations
            this.leftCodeMirror = codeMirrorComponent.instance;
            this.leftCodeMirror.setElementRef(codeMirrorComponent);
            this.leftCodeMirror.setClass(settings_component_1.SettingsComponent.showSecondCodeMirror() ? 'col-sm-6' : 'col-sm-12');
            if (settings_component_1.SettingsComponent.showSecondCodeMirror()) {
                codeMirrorComponent = this.codeMirrorContainer.createComponent(comp);
                // see explanations
                this.rightCodeMirror = codeMirrorComponent.instance;
                this.rightCodeMirror.setElementRef(codeMirrorComponent);
                this.rightCodeMirror.setClass('col-sm-6');
            }
        }
    };
    ContentComponent.prototype.setComponentSize = function () {
        var headerHeight = $('.main-header').first().outerHeight(true);
        var footerHeight = $('.main-footer').first().outerHeight(true);
        var content = $('.content').first();
        var contentPadding = content.innerHeight() - content.first().height();
        var filesBarHeight = $('#files-chooser').outerHeight(true);
        var fullHeight = document.body.offsetHeight - (headerHeight + footerHeight +
            contentPadding + filesBarHeight + Constants_1.Constants.SVG_MARGIN + 5); // don't know where to find these 5 last pixels...
        if (settings_component_1.SettingsComponent.onlyCode()) {
            this.leftCodeMirror.setPreferredHeight(fullHeight);
            if (settings_component_1.SettingsComponent.showDiff()) {
                this.rightCodeMirror.setPreferredHeight(fullHeight);
            }
        }
        else if (settings_component_1.SettingsComponent.onlyAst()) {
            this.leftTree.setPreferredHeight(fullHeight);
            if (settings_component_1.SettingsComponent.showDiff()) {
                this.rightTree.setPreferredHeight(fullHeight);
            }
        }
        else {
            this.leftCodeMirror.setPreferredHeight(fullHeight * this.codeMirrorHeightPercentage / 100);
            this.rightCodeMirror.setPreferredHeight(fullHeight * this.codeMirrorHeightPercentage / 100);
            this.leftTree.setPreferredHeight(fullHeight * (100 - this.codeMirrorHeightPercentage) / 100);
            this.rightTree.setPreferredHeight(fullHeight * (100 - this.codeMirrorHeightPercentage) / 100);
        }
    };
    ContentComponent.prototype.updateScroll = function (scrollInfo, caller) {
        if (settings_component_1.SettingsComponent.syncScrollEnabled() && settings_component_1.SettingsComponent.showDiff()) {
            if (this.leftCodeMirror === caller) {
                this.rightCodeMirror.scrollTo(scrollInfo);
            }
            else {
                this.leftCodeMirror.scrollTo(scrollInfo);
            }
        }
    };
    ContentComponent.prototype.setDiff = function () {
        this.leftTreeData.markDiff(this.leftCodeMirror);
        this.rightTreeData.markDiff(this.rightCodeMirror);
    };
    ContentComponent.prototype.onKeyDown = function (event) {
        if (event.key == 'e') {
            if (this.leftTree != null && this.rightTree != null) {
                this.leftTree.expandAll();
                this.rightTree.expandAll();
            }
            else if (this.leftTree != null) {
                this.leftTree.expandAll();
            }
        }
        else if (event.key == 'c') {
            if (this.leftTree != null && this.rightTree != null) {
                this.leftTree.collapseAll();
                this.rightTree.collapseAll();
            }
            else if (this.leftTree != null) {
                this.leftTree.collapseAll();
            }
        }
    };
    __decorate([
        core_1.ViewChild('treeParent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], ContentComponent.prototype, "treeContainer", void 0);
    __decorate([
        core_1.ViewChild('codeMirrorParent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], ContentComponent.prototype, "codeMirrorContainer", void 0);
    __decorate([
        core_1.ViewChild('filechooserComponent'),
        __metadata("design:type", filechooser_component_1.FilechooserComponent)
    ], ContentComponent.prototype, "filechooserComponent", void 0);
    __decorate([
        core_1.HostListener('document:keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], ContentComponent.prototype, "onKeyDown", null);
    ContentComponent = __decorate([
        core_1.Component({
            selector: 'app-content',
            templateUrl: './content.component.html',
            styleUrls: ['./content.component.css']
        }),
        __metadata("design:paramtypes", [core_1.ComponentFactoryResolver])
    ], ContentComponent);
    return ContentComponent;
}());
exports.ContentComponent = ContentComponent;
//# sourceMappingURL=content.component.js.map