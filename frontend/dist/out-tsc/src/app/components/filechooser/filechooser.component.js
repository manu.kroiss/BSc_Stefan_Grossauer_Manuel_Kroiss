"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_component_1 = require("../../app.component");
var FilechooserComponent = /** @class */ (function () {
    function FilechooserComponent() {
        this.diff = true;
        this.sourceFiles = app_component_1.AppComponent.getSourceFiles();
        if (this.sourceFiles.length > 0) {
            this.selectedFile1 = this.sourceFiles[0];
            this.selectedFile2 = this.sourceFiles[0];
            app_component_1.AppComponent.selectedFile1 = this.sourceFiles[0];
            app_component_1.AppComponent.selectedFile2 = this.sourceFiles[0];
        }
    }
    FilechooserComponent.prototype.ngOnInit = function () {
    };
    FilechooserComponent.prototype.setContentComponent = function (contentComponent) {
        this.contentComponent = contentComponent;
    };
    FilechooserComponent.prototype.onSelectChange1 = function (newFile) {
        this.selectedFile1 = newFile;
        app_component_1.AppComponent.selectedFile1 = newFile;
        this.contentComponent.init();
    };
    FilechooserComponent.prototype.onSelectChange2 = function (newFile) {
        this.selectedFile2 = newFile;
        app_component_1.AppComponent.selectedFile2 = newFile;
        this.contentComponent.init();
    };
    FilechooserComponent.prototype.updateDiff = function (diff) {
        this.diff = diff;
    };
    FilechooserComponent = __decorate([
        core_1.Component({
            selector: 'app-filechooser',
            templateUrl: './filechooser.component.html',
            styleUrls: ['./filechooser.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], FilechooserComponent);
    return FilechooserComponent;
}());
exports.FilechooserComponent = FilechooserComponent;
//# sourceMappingURL=filechooser.component.js.map