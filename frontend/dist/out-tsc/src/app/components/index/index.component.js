"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var settings_component_1 = require("../settings/settings.component");
var content_component_1 = require("../content/content.component");
var appheader_component_1 = require("../appheader/appheader.component");
var IndexComponent = /** @class */ (function () {
    function IndexComponent() {
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.ngAfterViewInit = function () {
        this.headerComponent.setContentComponent(this.contentComponent);
        this.settingsComponent.setContentComponent(this.contentComponent);
    };
    __decorate([
        core_1.ViewChild('headerComponent'),
        __metadata("design:type", appheader_component_1.AppheaderComponent)
    ], IndexComponent.prototype, "headerComponent", void 0);
    __decorate([
        core_1.ViewChild('contentComponent'),
        __metadata("design:type", content_component_1.ContentComponent)
    ], IndexComponent.prototype, "contentComponent", void 0);
    __decorate([
        core_1.ViewChild('settingsComponent'),
        __metadata("design:type", settings_component_1.SettingsComponent)
    ], IndexComponent.prototype, "settingsComponent", void 0);
    __decorate([
        core_1.ViewChild('contentParent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], IndexComponent.prototype, "contentComponentContainer", void 0);
    IndexComponent = __decorate([
        core_1.Component({
            selector: 'app-index',
            templateUrl: './index.component.html',
            styleUrls: ['./index.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], IndexComponent);
    return IndexComponent;
}());
exports.IndexComponent = IndexComponent;
//# sourceMappingURL=index.component.js.map