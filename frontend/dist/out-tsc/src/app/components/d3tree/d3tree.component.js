"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var d3 = require("d3");
var http_1 = require("@angular/common/http");
var Constants_1 = require("../../utils/Constants");
var settings_component_1 = require("../settings/settings.component");
var D3treeComponent = /** @class */ (function () {
    function D3treeComponent(http, el) {
        this.http = http;
        this.el = el;
        this.timeout = 500;
        this.nodeSize = 10;
        this.zero = 1e-6;
        this.codemirrorPadding = 30;
        this.margin = {
            top: 20,
            right: 120,
            bottom: 20,
            left: 120
        };
        this.duration = 750;
        this.i = 0; // ids for the nodes
        this.headerHeight = 50;
        this.htmlDomElement = this.el.nativeElement;
    }
    D3treeComponent_1 = D3treeComponent;
    D3treeComponent.prototype.setElementRef = function (elementRef) {
        this.elementRef = elementRef;
    };
    D3treeComponent.prototype.ngOnInit = function () {
        this.componentId = D3treeComponent_1.id;
        D3treeComponent_1.id++;
    };
    D3treeComponent.resetIdCounter = function () {
        D3treeComponent_1.id = 1;
    };
    D3treeComponent.prototype.ngAfterViewInit = function () {
    };
    D3treeComponent.prototype.reset = function () {
        while (this.htmlDomElement.firstChild) {
            this.htmlDomElement.removeChild(this.htmlDomElement.firstChild);
        }
    };
    D3treeComponent.prototype.initTree = function (contentComponent, data, orientation) {
        var _this = this;
        this.contentComponent = contentComponent;
        this.reset();
        this.orientation = orientation;
        // ************** Generate the tree diagram	 *****************
        setTimeout(function () {
            _this.createTree(data);
        }, this.timeout);
    };
    D3treeComponent.prototype.collapse = function (d) {
        var _this = this;
        if (d.children) {
            d._children = d.children;
            d._children.forEach(function (elem) {
                _this.collapse(elem);
            });
            d.children = null;
        }
    };
    D3treeComponent.prototype.expand = function (d) {
        var _this = this;
        if (d._children) {
            d.children = d._children;
            d.children.forEach(function (elem) {
                _this.expand(elem);
            });
            d._children = null;
        }
    };
    D3treeComponent.prototype.expandAll = function () {
        var _this = this;
        this.root.children.forEach(function (elem) {
            _this.expand(elem);
        });
        this.update(this.root);
    };
    D3treeComponent.prototype.collapseAll = function () {
        var _this = this;
        this.root.children.forEach(function (elem) {
            _this.collapse(elem);
        });
        this.update(this.root);
    };
    D3treeComponent.prototype.createTree = function (data) {
        var _this = this;
        this.levelGap = this.orientation == 'vertical' ? 130 : 150;
        this.horizontaltextOffset = this.orientation == 'vertical' ? 18 : 13;
        this.verticalTextOffset = this.orientation == 'vertical' ? 18 : 18;
        var clientWidth = this.htmlDomElement.getBoundingClientRect().width;
        var width = clientWidth - this.margin.right - this.margin.left - this.codemirrorPadding;
        var height = (this.preferredHeight != null ? this.preferredHeight : document.body.offsetHeight / 2 - this.headerHeight) - this.margin.top - this.margin.bottom;
        this.treeLayout = this.orientation == 'vertical' ? d3.tree().size([width, height]) : d3.tree().size([height, width]);
        this.svg = d3.select(this.htmlDomElement).append('svg')
            .attr('style', 'margin-top: ' + Constants_1.Constants.SVG_MARGIN + 'px')
            .attr('class', 'border svg' + this.componentId)
            .attr('width', width + this.margin.right + this.margin.left)
            .attr('height', height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('class', 'drawarea' + this.componentId)
            .append('g')
            .attr('transform', 'translate(' + (this.margin.left) + ',' + this.margin.top + ')');
        this.root = d3.hierarchy(data, function (d) {
            return d.children;
        });
        this.root.x0 = this.orientation == 'vertical' ? 0 : height / 2;
        this.root.y0 = this.orientation == 'vertical' ? width / 2 : 0;
        this.root.children.forEach(function (elem) {
            _this.collapse(elem);
        });
        this.update(this.root);
        //Add zoom extension
        d3.select('.svg' + this.componentId).call(d3.zoom()
            .scaleExtent([0.5, 5])
            .on('zoom', function () { return _this.zoom(width, height); }));
    };
    D3treeComponent.prototype.zoom = function (width, height) {
        var transform = d3.event.transform;
        var scale = transform.k;
        var scaleFactor = 2;
        var drawAreaWidth = document.getElementsByClassName('drawarea' + this.componentId).item(0).getBoundingClientRect().width;
        var tBound = this.orientation == 'vertical' ? -height * scale + scaleFactor * scale : -height * scale + scaleFactor * scale; // top
        var bBound = height - scaleFactor * scale; // bottom
        var lBound = -drawAreaWidth * scale + scaleFactor * scale; // left
        var rBound = width - scaleFactor * scale; // right
        transform.x = Math.max(Math.min(transform.x, rBound), lBound);
        transform.y = Math.max(Math.min(transform.y, bBound), tBound);
        d3.select('.drawarea' + this.componentId)
            .attr('transform', transform);
        if (settings_component_1.SettingsComponent.syncZoomEnabled()) {
            if ('.drawarea' + this.componentId === '.drawarea1') {
                d3.select('.drawarea2')
                    .attr('transform', transform);
            }
            else {
                d3.select('.drawarea1')
                    .attr('transform', transform);
            }
        }
    };
    // Creates a curved (diagonal) path from parent to the child nodes
    D3treeComponent.prototype.diagonal = function (s, d) {
        if (this.orientation == 'vertical') {
            return "M " + s.x + " " + s.y + "\n          C " + (s.x + d.x) / 2 + " " + s.y + ",\n            " + (s.x + d.x) / 2 + " " + d.y + ",\n            " + d.x + " " + d.y;
        }
        else {
            return "M " + s.y + " " + s.x + "\n              C " + (s.y + d.y) / 2 + " " + s.x + ",\n                " + (s.y + d.y) / 2 + " " + d.x + ",\n                " + d.y + " " + d.x;
        }
    };
    D3treeComponent.prototype.appendText = function (nodeEnter, attribute, line) {
        var _this = this;
        var text = nodeEnter.append('text')
            .attr('x', function (d) {
            if (_this.orientation == 'vertical') {
                return 0;
            }
            else {
                // check whether node has children or not
                return d.children || d._children ? -_this.horizontaltextOffset : _this.horizontaltextOffset;
            }
        })
            .attr('y', function (d) {
            if (_this.orientation == 'vertical') {
                return line * _this.verticalTextOffset;
            }
            else {
                return d.children || d._children
                    ? (d.depth % 2 == 0 ? -(2 - line) * _this.verticalTextOffset : line * _this.verticalTextOffset + _this.verticalTextOffset)
                    : line * _this.verticalTextOffset;
            }
        })
            .attr('dy', '.35em')
            .attr('text-anchor', function (d) {
            if (_this.orientation == 'vertical') {
                return 'end';
            }
            else {
                return d.children || d._children ? 'end' : 'start';
            }
        })
            .style('fill-opacity', this.zero);
        if (attribute == 'name') {
            text.text(function (d) {
                return d.data.name;
            })
                .attr('visibility', function (d) {
                return d.data.name == null ? 'hidden' : 'visible';
            });
        }
        else if (attribute == 'type') {
            text.text(function (d) {
                return '<<' + d.data.type + '>>' + (d.data.operator == null ? '' : ' : ' + d.data.operator) + (d.data.value == null ? '' : ': ' + d.data.value);
            })
                .attr('visibility', function (d) {
                return d.data.type == null ? 'hidden' : 'visible';
            });
        }
    };
    D3treeComponent.prototype.update = function (source) {
        var _this = this;
        // Compute the new tree layout.
        var treeData = this.treeLayout(this.root);
        var nodes = treeData.descendants();
        var links = treeData.descendants().slice(1);
        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * _this.levelGap;
        });
        // Update the nodes…
        var node = this.svg.selectAll('g.node')
            .data(nodes, function (d) {
            return d.id || (d.id = ++_this.i);
        });
        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append('g')
            .attr('class', 'node cursor')
            .attr('transform', function () {
            if (_this.orientation == 'vertical') {
                return 'translate(' + source.x0 + ',' + source.y0 + ')';
            }
            else {
                return 'translate(' + source.y0 + ',' + source.x0 + ')';
            }
        })
            .on('click', function (d) { return _this.click(d); });
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', this.zero)
            .style('fill', function (d) { return D3treeComponent_1.styleNode(d); });
        this.appendText(nodeEnter, 'type', 0);
        this.appendText(nodeEnter, 'name', 1);
        if (this.orientation == 'vertical') {
            d3.selectAll('g text').attr('transform', 'translate(-10,15) rotate(-90)');
        }
        // Transition nodes to their new position.
        var nodeUpdate = nodeEnter.merge(node);
        nodeUpdate.transition()
            .duration(this.duration)
            .attr('transform', function (d) {
            if (_this.orientation == 'vertical') {
                return 'translate(' + d.x + ',' + d.y + ')';
            }
            else {
                return 'translate(' + d.y + ',' + d.x + ')';
            }
        });
        nodeUpdate.select('circle.node')
            .attr('r', this.nodeSize)
            .style('fill', function (d) { return D3treeComponent_1.styleNode(d); });
        nodeUpdate.selectAll('g text')
            .style('fill-opacity', 1);
        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(this.duration)
            .attr('transform', function () {
            if (_this.orientation == 'vertical') {
                return 'translate(' + source.x + ',' + source.y + ')';
            }
            else {
                return 'translate(' + source.y + ',' + source.x + ')';
            }
        })
            .remove();
        nodeExit.select('circle')
            .attr('r', this.zero);
        nodeExit.select('text')
            .style('fill-opacity', this.zero);
        // Update the links…
        var link = this.svg.selectAll('path.link')
            .data(links, function (d) {
            return d.id;
        });
        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert('path', 'g')
            .attr('class', 'link')
            .attr('d', function () {
            if (_this.orientation == 'vertical') {
                var o = { y: source.y0, x: source.x0 };
                return _this.diagonal(o, o);
            }
            else {
                var o = { x: source.x0, y: source.y0 };
                return _this.diagonal(o, o);
            }
        });
        // Transition links to their new position.
        var linkUpdate = linkEnter.merge(link);
        linkUpdate.transition()
            .duration(this.duration)
            .attr('d', function (d) {
            if (_this.orientation == 'vertical') {
                return _this.diagonal(d, d.parent);
            }
            else {
                return _this.diagonal(d.parent, d);
            }
        });
        // Transition exiting links to the parent's new position.
        link.exit().transition()
            .duration(this.duration)
            .attr('d', function () {
            var o = { x: source.x, y: source.y };
            return _this.diagonal(o, o);
        })
            .remove();
        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    };
    D3treeComponent.styleNode = function (d) {
        if (d.data.diff) {
            return d._children ? '#fc6b5d' : '#ffb5a8';
        }
        else {
            return d._children ? 'lightsteelblue' : '#ffffff';
        }
    };
    D3treeComponent.prototype.click = function (d) {
        this.collapseExpand(d);
        if (settings_component_1.SettingsComponent.syncCollapseEnabled()) {
            this.contentComponent.updateNode(d.data.hashcode, this);
        }
    };
    D3treeComponent.prototype.collapseExpand = function (d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        }
        else {
            d.children = d._children;
            d._children = null;
        }
        this.update(d);
    };
    D3treeComponent.prototype.updateNode = function (hashcode) {
        var syncNode = this.getSyncNode(hashcode, this.root);
        if (syncNode != null) {
            this.collapseExpand(syncNode);
        }
    };
    D3treeComponent.prototype.getSyncNode = function (hashcode, node) {
        if (node == null)
            return;
        if (node.data.hashcode === hashcode) {
            return node;
        }
        if (node.children != null) {
            for (var i = 0; i < node.children.length; i++) {
                var syncNode = this.getSyncNode(hashcode, node.children[i]);
                if (syncNode != null)
                    return syncNode;
            }
        }
        if (node._children != null) {
            for (var i = 0; i < node._children.length; i++) {
                var syncNode = this.getSyncNode(hashcode, node._children[i]);
                if (syncNode != null)
                    return syncNode;
            }
        }
        return null;
    };
    D3treeComponent.prototype.destroy = function () {
        this.elementRef.destroy();
    };
    D3treeComponent.prototype.setClass = function (className) {
        this.htmlDomElement.setAttribute('class', className);
    };
    D3treeComponent.prototype.setPreferredHeight = function (height) {
        this.preferredHeight = height;
    };
    D3treeComponent.id = 1;
    D3treeComponent = D3treeComponent_1 = __decorate([
        core_1.Component({
            selector: 'app-d3tree',
            templateUrl: './d3tree.component.html',
            styleUrls: ['./d3tree.component.css']
        }),
        __metadata("design:paramtypes", [http_1.HttpClient,
            core_1.ElementRef])
    ], D3treeComponent);
    return D3treeComponent;
    var D3treeComponent_1;
}());
exports.D3treeComponent = D3treeComponent;
//# sourceMappingURL=d3tree.component.js.map