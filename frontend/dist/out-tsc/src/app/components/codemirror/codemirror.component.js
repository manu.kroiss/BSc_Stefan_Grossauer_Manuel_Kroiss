"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// import 'codemirror/mode/javascript/javascript';
var CodeMirror = require("codemirror");
var CodemirrorComponent = /** @class */ (function () {
    function CodemirrorComponent(el) {
        this.el = el;
        this.userScrolled = true;
        this.htmlDomElement = el.nativeElement;
        Promise.resolve().then(function () { return require('codemirror/mode/clike/clike'); });
    }
    CodemirrorComponent.prototype.ngOnInit = function () {
    };
    CodemirrorComponent.prototype.setPreferredHeight = function (height) {
        this.preferredHeight = height;
    };
    CodemirrorComponent.prototype.markDiff = function (from, to, fromChar, toChar) {
        this.cm.markText({ line: from, ch: fromChar }, { line: to, ch: toChar + 1 }, { className: 'myBg' });
    };
    CodemirrorComponent.prototype.initCodeMirror = function (contentComponent, code) {
        var _this = this;
        this.contentComponent = contentComponent;
        this.cm = CodeMirror(this.htmlDomElement, {
            lineNumbers: true,
            readOnly: true,
        });
        this.cm.setSize(null, this.preferredHeight != null ? this.preferredHeight : '100%');
        this.cm.replaceRange(code, {
            line: Infinity
        });
        this.cm.setSelection({
            'line': this.cm.firstLine(),
            'ch': 0,
            'sticky': null
        }, {
            'line': this.cm.lastLine(),
            'ch': 0,
            'sticky': null
        }, {
            scroll: true
        });
        // auto indent the selection
        // this.cm.indentSelection('smart');
        this.cm.setSelection({
            'line': this.cm.firstLine(),
            'ch': 0,
            'sticky': null
        }, {
            'line': this.cm.firstLine(),
            'ch': 0,
            'sticky': null
        }, {
            scroll: true
        });
        this.cm.on('scroll', function (instance) { return _this.onScroll(instance); }, { passive: true });
    };
    CodemirrorComponent.prototype.onScroll = function (instance) {
        if (this.userScrolled) {
            this.contentComponent.updateScroll(instance.getScrollInfo(), this);
        }
        else {
            this.userScrolled = true;
        }
    };
    CodemirrorComponent.prototype.setClass = function (className) {
        this.htmlDomElement.setAttribute('class', className);
    };
    CodemirrorComponent.prototype.setElementRef = function (elementRef) {
        this.elementRef = elementRef;
    };
    CodemirrorComponent.prototype.scrollTo = function (scrollInfo) {
        this.userScrolled = false;
        this.cm.scrollTo(scrollInfo.left, scrollInfo.top);
    };
    CodemirrorComponent = __decorate([
        core_1.Component({
            selector: 'app-codemirror',
            templateUrl: './codemirror.component.html',
            styleUrls: ['./codemirror.component.css']
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], CodemirrorComponent);
    return CodemirrorComponent;
}());
exports.CodemirrorComponent = CodemirrorComponent;
//# sourceMappingURL=codemirror.component.js.map