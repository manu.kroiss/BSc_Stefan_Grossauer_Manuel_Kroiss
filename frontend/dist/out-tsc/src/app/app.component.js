"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Language_1 = require("./models/Language");
var http_1 = require("@angular/common/http");
var AppComponent = /** @class */ (function () {
    function AppComponent(http) {
        this.http = http;
        this.http.get('./assets/res/supported_languages.json').subscribe(function (res) {
            var tmp = res;
            tmp.supportedLanguages.forEach(function (lan) {
                AppComponent_1._supportedLanguages.push(new Language_1.Language(lan, false));
            });
            AppComponent_1._supportedLanguages[0].selected = true;
        }, function (error) {
            throw error;
        });
    }
    AppComponent_1 = AppComponent;
    AppComponent.getSourceFiles = function () {
        return this.sourceFiles;
    };
    Object.defineProperty(AppComponent, "selectedProgrammingLanguage", {
        get: function () {
            for (var i = 0; i < this._supportedLanguages.length; i++) {
                if (this._supportedLanguages[i].selected)
                    return this._supportedLanguages[i];
            }
            return null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "selectedFile1", {
        get: function () {
            return this._selectedFile1;
        },
        set: function (value) {
            this._selectedFile1 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "selectedFile2", {
        get: function () {
            return this._selectedFile2;
        },
        set: function (value) {
            this._selectedFile2 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "synchronousZoom", {
        get: function () {
            return this._synchronousZoom;
        },
        set: function (value) {
            this._synchronousZoom = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "synchronousCollapse", {
        get: function () {
            return this._synchronousCollapse;
        },
        set: function (value) {
            this._synchronousCollapse = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "synchronousScroll", {
        get: function () {
            return this._synchronousScroll;
        },
        set: function (value) {
            this._synchronousScroll = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "horizontalTree", {
        get: function () {
            return this._horizontalTree;
        },
        set: function (value) {
            this._horizontalTree = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "supportedLanguages", {
        get: function () {
            return this._supportedLanguages;
        },
        set: function (value) {
            this._supportedLanguages = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppComponent, "viewOptions", {
        get: function () {
            return this._viewOptions;
        },
        set: function (value) {
            this._viewOptions = value;
        },
        enumerable: true,
        configurable: true
    });
    AppComponent.sourceFiles = [];
    // private static _selectedProgrammingLanguage: Language;
    AppComponent._selectedFile1 = null;
    AppComponent._selectedFile2 = null;
    AppComponent._synchronousZoom = true;
    AppComponent._synchronousCollapse = true;
    AppComponent._synchronousScroll = true;
    AppComponent._horizontalTree = true;
    AppComponent._supportedLanguages = [];
    AppComponent._viewOptions = {
        codeAndAstDiff: true,
        codeDiff: false,
        astDiff: false,
        codeOnly: false,
        astOnly: false
    };
    AppComponent = AppComponent_1 = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: '<router-outlet></router-outlet>'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AppComponent);
    return AppComponent;
    var AppComponent_1;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map