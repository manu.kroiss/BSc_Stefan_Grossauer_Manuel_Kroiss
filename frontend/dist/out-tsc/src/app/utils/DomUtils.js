"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DomUtils = /** @class */ (function () {
    function DomUtils() {
    }
    DomUtils.removeIElementChildren = function (parent) {
        var chidlren = parent.children;
        for (var i = 0; i < chidlren.length; i++) {
            if (chidlren[i].tagName.toLowerCase() === 'i') {
                parent.removeChild(chidlren[i]);
            }
        }
    };
    DomUtils.removeChildren = function (parent) {
        var chidlren = parent.children;
        for (var i = 0; i < chidlren.length; i++) {
            parent.removeChild(chidlren[i]);
        }
    };
    return DomUtils;
}());
exports.DomUtils = DomUtils;
//# sourceMappingURL=DomUtils.js.map