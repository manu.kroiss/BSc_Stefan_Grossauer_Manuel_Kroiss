"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StringUtil = /** @class */ (function () {
    function StringUtil() {
    }
    StringUtil.getStringHash = function (string) {
        if (string == null) {
            return 0;
        }
        var hash = string.length * 7;
        for (var i = 0; i < string.length; i++) {
            hash += string.charCodeAt(i) * 3;
        }
        return hash;
    };
    return StringUtil;
}());
exports.StringUtil = StringUtil;
//# sourceMappingURL=StringUtil.js.map