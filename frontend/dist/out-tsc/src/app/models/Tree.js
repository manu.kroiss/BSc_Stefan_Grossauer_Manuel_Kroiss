"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// import {ITree} from './ITree';
var StringUtil_1 = require("../utils/StringUtil");
var Tree = /** @class */ (function () {
    // private data: any;
    function Tree(data) {
        var _this = this;
        this._diff = false;
        this._codeMirrorDiff = false;
        this.root = data;
        // this.data = data;
        this._children = [];
        if (this.root.children) {
            this.root.children.forEach(function (child) {
                _this._children[_this._children.length] = new Tree(child);
            });
        }
    }
    Object.defineProperty(Tree.prototype, "codeMirrorDiff", {
        get: function () {
            return this._codeMirrorDiff;
        },
        set: function (value) {
            this._codeMirrorDiff = value;
        },
        enumerable: true,
        configurable: true
    });
    Tree.prototype.getName = function () {
        return this.root.name;
    };
    Tree.prototype.getType = function () {
        return this.root.type;
    };
    Tree.prototype.getValue = function () {
        return this.root.value;
    };
    Tree.prototype.getOperator = function () {
        return this.root.operator;
    };
    Tree.prototype.getStartLine = function () {
        return this.root.startLine;
    };
    Tree.prototype.getEndLine = function () {
        return this.root.endLine;
    };
    Tree.prototype.getStartChar = function () {
        return this.root.startChar == null ? 0 : this.root.startChar;
    };
    Tree.prototype.getEndChar = function () {
        return this.root.endChar == null ? 1 : this.root.endChar;
    };
    Object.defineProperty(Tree.prototype, "diff", {
        get: function () {
            return this._diff;
        },
        set: function (value) {
            this._diff = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tree.prototype, "children", {
        get: function () {
            return this._children;
        },
        enumerable: true,
        configurable: true
    });
    Tree.prototype.hashcode = function () {
        var hash = StringUtil_1.StringUtil.getStringHash(this.getName());
        hash += StringUtil_1.StringUtil.getStringHash(this.getType());
        hash += this.getValue();
        hash += StringUtil_1.StringUtil.getStringHash(this.getOperator());
        hash += this._children.length;
        this._children.forEach(function (child) {
            hash += child.hashcode();
        });
        return hash;
    };
    Tree.prototype.parentHashcode = function () {
        var hash = StringUtil_1.StringUtil.getStringHash(this.getName());
        hash += StringUtil_1.StringUtil.getStringHash(this.getType());
        hash += this.getValue();
        hash += StringUtil_1.StringUtil.getStringHash(this.getOperator());
        return hash;
    };
    Tree.prototype.getAsITreeObject = function () {
        return {
            type: this.getType(),
            name: this.getName(),
            value: this.getValue(),
            operator: this.getOperator(),
            startLine: this.getStartLine(),
            endLine: this.getEndLine(),
            startChar: this.getStartChar(),
            endChar: this.getEndChar(),
            hashcode: this._similarHash != null ? this._similarHash : this.hashcode(),
            diff: this._diff,
            children: this.getChildrenAsObject()
        };
    };
    Tree.prototype.getChildrenAsObject = function () {
        var children = [];
        this._children.forEach(function (child) {
            children.push(child.getAsITreeObject());
        });
        return children;
    };
    Object.defineProperty(Tree.prototype, "similarHash", {
        set: function (value) {
            this._similarHash = value;
        },
        enumerable: true,
        configurable: true
    });
    Tree.prototype.markDiff = function (codeMirror) {
        if (this._codeMirrorDiff) {
            if (this.root.startLine != null && this.root.endLine != null) {
                codeMirror.markDiff(this.root.startLine - 1, this.root.endLine - 1, this.getStartChar(), this.getEndChar());
            }
        }
        this._children.forEach(function (child) { return child.markDiff(codeMirror); });
    };
    return Tree;
}());
exports.Tree = Tree;
//# sourceMappingURL=Tree.js.map