"use strict";
/**
 * Abstract Syntax Tree Generation
 * This is a server for generating Abstract Syntax Trees (ASTs) from a given source code file. You can find out more about it at [https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss](   https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss).
 *
 * OpenAPI spec version: 0.1.0
 * Contact: e1525664@student.tuwien.ac.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var AstNode;
(function (AstNode) {
    AstNode.TypeEnum = {
        Root: 'root',
        Class: 'class',
        FunctionDeclaration: 'function_declaration',
        Parameters: 'parameters',
        Parameter: 'parameter',
        If: 'if',
        Condition: 'condition',
        Then: 'then',
        Else: 'else',
        Expression: 'expression',
        Statement: 'statement',
        BinaryExpression: 'binary_expression',
        Variable: 'variable',
        Decimal: 'decimal',
        Float: 'float',
        Return: 'return',
        FunctionCall: 'function_call'
    };
})(AstNode = exports.AstNode || (exports.AstNode = {}));
//# sourceMappingURL=AstNode.js.map