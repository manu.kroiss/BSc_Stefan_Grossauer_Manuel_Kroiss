"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SourceFile = /** @class */ (function () {
    function SourceFile(filename, name, uploadTime, content, tree) {
        this._filename = filename;
        this._identifier = name;
        this._lastModifiedDate = uploadTime;
        this._content = content;
        this._tree = tree;
    }
    Object.defineProperty(SourceFile.prototype, "filename", {
        get: function () {
            return this._filename;
        },
        set: function (value) {
            this._filename = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SourceFile.prototype, "identifier", {
        get: function () {
            return this._identifier;
        },
        set: function (value) {
            this._identifier = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SourceFile.prototype, "lastModifiedDate", {
        get: function () {
            return this._lastModifiedDate;
        },
        set: function (value) {
            this._lastModifiedDate = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SourceFile.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (value) {
            this._content = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SourceFile.prototype, "tree", {
        get: function () {
            return this._tree;
        },
        set: function (value) {
            this._tree = value;
        },
        enumerable: true,
        configurable: true
    });
    return SourceFile;
}());
exports.SourceFile = SourceFile;
//# sourceMappingURL=SourceFile.js.map