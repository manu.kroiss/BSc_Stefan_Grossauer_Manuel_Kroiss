/**
 * Abstract Syntax Tree Generation
 * This is a server for generating Abstract Syntax Trees (ASTs) from a given source code file. You can find out more about it at [https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss](   https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss).
 *
 * OpenAPI spec version: 0.1.0
 * Contact: e1525664@student.tuwien.ac.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import {Inject, Injectable, Optional} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';

import {AstNode} from '../../models/AstNode';
import {Configuration} from './Configuration';
import {BASE_PATH} from './variables';
import {CustomHttpUrlEncodingCodec} from './encoder';


@Injectable()
export class AstService {

  protected basePath = 'http://localhost:8080';
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();

  constructor(protected httpClient: HttpClient, @Optional() @Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
    if (basePath) {
      this.basePath = basePath;
    }
    if (configuration) {
      this.configuration = configuration;
      this.basePath = basePath || configuration.basePath || this.basePath;
    }
  }

  /**
   * @param consumes string[] mime-types
   * @return true: consumes contains 'multipart/form-data', false: otherwise
   */
  private static canConsumeForm(consumes: string[]): boolean {
    const form = 'multipart/form-data';
    for (let consume of consumes) {
      if (form === consume) {
        return true;
      }
    }
    return false;
  }


  /**
   * All about the summary
   * Straight forward
   * @param programmingLanguage The programming language used for the source code
   * @param sourceCodeFile Source code file
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public ast(programmingLanguage: string, sourceCodeFile: Blob, observe?: 'body', reportProgress?: boolean): Observable<AstNode>;
  public ast(programmingLanguage: string, sourceCodeFile: Blob, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<AstNode>>;
  public ast(programmingLanguage: string, sourceCodeFile: Blob, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<AstNode>>;
  public ast(programmingLanguage: string, sourceCodeFile: Blob, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
    if (programmingLanguage === null || programmingLanguage === undefined) {
      throw new Error('Required parameter programmingLanguage was null or undefined when calling ast.');
    }
    if (sourceCodeFile === null || sourceCodeFile === undefined) {
      throw new Error('Required parameter sourceCodeFile was null or undefined when calling ast.');
    }

    let headers = this.defaultHeaders;

    // to determine the Accept header
    let httpHeaderAccepts: string[] = [
      'application/json'
    ];
    let httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    if (httpHeaderAcceptSelected != undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }

    // to determine the Content-Type header
    let consumes: string[] = [
      'multipart/form-data'
    ];

    const canConsumeForm = AstService.canConsumeForm(consumes);

    let formParams: { append(param: string, value: any): void; };
    let useForm: boolean;
    let convertFormParamsToString = false;
    // use FormData to transmit files using content-type "multipart/form-data"
    // see https://stackoverflow.com/questions/4007969/application-x-www-form-urlencoded-or-multipart-form-data
    useForm = canConsumeForm;
    if (useForm) {
      formParams = new FormData();
    } else {
      formParams = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
    }

    if (programmingLanguage !== undefined) {
      formParams = formParams.append('programming_language', <any>programmingLanguage) || formParams;
    }
    if (sourceCodeFile !== undefined) {
      formParams = formParams.append('source_code_file', <any>sourceCodeFile) || formParams;
    }

    return this.httpClient.post<AstNode>(`${this.basePath}/ast`,
      convertFormParamsToString ? formParams.toString() : formParams,
      {
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

}
