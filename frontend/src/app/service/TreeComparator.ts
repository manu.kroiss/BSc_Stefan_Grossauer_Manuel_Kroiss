import {Tree} from '../models/Tree';

export class TreeComparator {

  /**
   * Extracts the similar nodes from the difference Array and stores the
   * similar nodes in the SimilarNodesArray
   *
   * After execution, the Difference Array only contains completely different nodes.
   *
   * @param {Array<Tree>} DifferenceArray
   * @returns {Array<Array<Tree>>} An array that contains an array (length 2) of similar nodes
   */
  splitSimilarAndStandaloneNodes(DifferenceArray: Array<Tree>): Array<Array<Tree>> {
    let similarNodesArray: Array<Array<Tree>> = [];

    DifferenceArray.forEach(d => {
      d.children.forEach(c => {
        DifferenceArray.filter(x => x != d).forEach(d2 => {
          d2.children.forEach(c2 => {
            if (c.hashcode() == c2.hashcode()) {
              similarNodesArray[similarNodesArray.length] = [];
              similarNodesArray[similarNodesArray.length - 1].push(d);
              similarNodesArray[similarNodesArray.length - 1].push(d2);
              DifferenceArray.splice(DifferenceArray.indexOf(d), 1);
              DifferenceArray.splice(DifferenceArray.indexOf(d2), 1);
              return similarNodesArray;
            }
          });
        });
      });
    });
    return similarNodesArray;
  }

  calcDiff(tree1: Tree, tree2: Tree): void {
    let hashArray: Array<Array<Tree>> = [];
    let children1: Array<Tree> = tree1.children;
    let children2: Array<Tree> = tree2.children;

    let differenceArray: Array<Tree> = [];

    if (children1.length == 1 && children2.length == 1) { // if the root nodes have only one child that are unequal
      if (children1[0].hashcode() != children2[0].hashcode()) { // nodes are similar
        children1[0].diff = true;
        children2[0].diff = true;
        children2[0].similarHash = children1[0].hashcode();
      }
      this.calcDiff(children1[0], children2[0]);
    } else { // find equal and unequal nodes
      children1.forEach(c => {
        if (!hashArray[c.hashcode()]) {
          hashArray[c.hashcode()] = [];
        }
        hashArray[c.hashcode()].push(c);
      });
      children2.forEach(c => {
        if (!hashArray[c.hashcode()]) {
          hashArray[c.hashcode()] = [];
        }
        hashArray[c.hashcode()].push(c);
      });
      hashArray.forEach(el => {
        if (el.length == 1) { // nodes are not equal
          el[0].diff = true;
          differenceArray.push(el[0]);
        }
        // else -> nodes are equal - no further processing required
      });

      let similarNodesArray: Array<Array<Tree>> = this.splitSimilarAndStandaloneNodes(differenceArray);

      // This root and its childs have no similar nodes
      differenceArray.forEach(d => {
        this.markDiffRec(d);
        d.codeMirrorDiff = true;
      });

      // compare all similar nodes
      similarNodesArray.forEach(p => {
        p[1].similarHash = p[0].hashcode(); // set fake hashcode for sync collapse of similar nodes
        this.calcDiff(p[0], p[1]);
      });
    }
  }

  /**
   * Marks the element and all elements children as different
   * @param {Tree} element the root element
   */
  private markDiffRec(element: Tree) {
    element.diff = true;
    element.children.forEach(c => {
      this.markDiffRec(c);
    });
  }
}
