export class DomUtils {

  public static removeIElementChildren(parent: HTMLElement): void {
    let chidlren: HTMLCollection = parent.children;
    for (let i = 0; i < chidlren.length; i++) {
      if (chidlren[i].tagName.toLowerCase() === 'i') {
        parent.removeChild(chidlren[i]);
      }
    }
  }

  public static removeChildren(parent: HTMLElement): void {
    let chidlren: HTMLCollection = parent.children;
    for (let i = 0; i < chidlren.length; i++) {
      parent.removeChild(chidlren[i]);
    }
  }
}
