export class StringUtil {
  public static getStringHash(string: String) : number {
    if (string == null) {
      return 0;
    }
    let hash: number = string.length * 7;
    for (let i = 0; i < string.length; i++) {
      hash += string.charCodeAt(i) * 3;
    }
    return hash;
  }
}
