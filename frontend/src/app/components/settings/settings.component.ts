import {Component, Input, OnInit} from '@angular/core';
import {ContentComponent} from '../content/content.component';
import {AppComponent} from '../../app.component';
import {DomUtils} from '../../utils/DomUtils';
import {Router} from '@angular/router';
import {Language} from '../../models/Language';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  private contentComponent: ContentComponent;
  horizontal: boolean = true;
  @Input() showSettings: boolean;

  supportedLanguages: Array<Language>;

  constructor(
    private router: Router
  ) {
    this.supportedLanguages = AppComponent.supportedLanguages;
  }

  liLanguageClick(data: string, index: number) {
    this.supportedLanguages.forEach(lan => {
      lan.selected = false;
    });
    this.supportedLanguages[index].selected = true;
  }

  ngOnInit() {
    this.horizontal = AppComponent.horizontalTree;
  }

  private static resetViewOptions(): void {
    AppComponent.viewOptions.codeAndAstDiff = false;
    AppComponent.viewOptions.codeDiff = false;
    AppComponent.viewOptions.astDiff = false;
    AppComponent.viewOptions.codeOnly = false;
    AppComponent.viewOptions.astOnly = false;
  }

  // noinspection JSMethodCanBeStatic
  synchronousZoomOptionClick() {
    AppComponent.synchronousZoom = !AppComponent.synchronousZoom;
    SettingsComponent.updateBooleanOption(document.getElementById('syncZoom').classList, AppComponent.synchronousZoom);
  }

  static updateBooleanOption(classList: DOMTokenList, enabled: boolean) {
    if (enabled) {
      classList.add('enabled');
    } else {
      classList.remove('enabled');
    }
  }

  // noinspection JSMethodCanBeStatic
  synchronousCollapseOptionClick() {
    AppComponent.synchronousCollapse = !AppComponent.synchronousCollapse;
    SettingsComponent.updateBooleanOption(document.getElementById('syncCollapse').classList, AppComponent.synchronousCollapse);
  }

  // noinspection JSMethodCanBeStatic
  synchronousScrollOptionClick(): void {
    AppComponent.synchronousScroll = !AppComponent.synchronousScroll;
    SettingsComponent.updateBooleanOption(document.getElementById('syncScroll').classList, AppComponent.synchronousScroll);
  }

  horizontalTreeOptionClick(): void {
    AppComponent.horizontalTree = true;
    this.horizontal = true;
    this.contentComponent.changeTreeOrientation('horizontal');
  }

  verticalTreeOptionClick() {
    AppComponent.horizontalTree = false;
    this.horizontal = false;
    this.contentComponent.changeTreeOrientation('vertical');

  }

  private static setActiveCheckSymbol(parent: HTMLElement, elementText: String): void {
    parent.innerHTML = '<span>' + elementText + '</span><i class="fa fa-check pull-right"></i>';
  }

  codeAndAstDiffOptionClick(): void {
    SettingsComponent.resetViewOptions();
    AppComponent.viewOptions.codeAndAstDiff = true;
    SettingsComponent.updateViewOptionSymbol();
    if (this.contentComponent != null) this.contentComponent.init();
  }

  codeDiffOptionClick(): void {
    SettingsComponent.resetViewOptions();
    AppComponent.viewOptions.codeDiff = true;
    SettingsComponent.updateViewOptionSymbol();
    if (this.contentComponent != null) this.contentComponent.init();
  }

  astDiffOptionClick(): void {
    SettingsComponent.resetViewOptions();
    AppComponent.viewOptions.astDiff = true;
    SettingsComponent.updateViewOptionSymbol();
    if (this.contentComponent != null) this.contentComponent.init();
  }

  codeOnlyOptionClick(): void {
    SettingsComponent.resetViewOptions();
    AppComponent.viewOptions.codeOnly = true;
    SettingsComponent.updateViewOptionSymbol();
    if (this.contentComponent != null) this.contentComponent.init();
  }

  astOnlyOptionClick(): void {
    SettingsComponent.resetViewOptions();
    AppComponent.viewOptions.astOnly = true;
    SettingsComponent.updateViewOptionSymbol();
    if (this.contentComponent != null) this.contentComponent.init();
  }

  private static updateViewOptionSymbol(): void {
    SettingsComponent.resetContentOptionsMenu();
    if (AppComponent.viewOptions.astOnly) {
      SettingsComponent.setActiveCheckSymbol(document.getElementById('astOnlyOption'), 'Ast Only');
    }
    if (AppComponent.viewOptions.codeOnly) {
      SettingsComponent.setActiveCheckSymbol(document.getElementById('codeOnlyOption'), 'Code Only');
    }
    if (AppComponent.viewOptions.astDiff) {
      SettingsComponent.setActiveCheckSymbol(document.getElementById('astDiffOption'), 'AST Diff');
    }
    if (AppComponent.viewOptions.codeAndAstDiff) {
      SettingsComponent.setActiveCheckSymbol(document.getElementById('codeAndAstDiffOption'), 'Code + AST Diff');
    }
    if (AppComponent.viewOptions.codeDiff) {
      SettingsComponent.setActiveCheckSymbol(document.getElementById('codeDiffOption'), 'Code Diff');
    }
  }

  public static reinitializeSettings() {
    SettingsComponent.updateViewOptionSymbol();
    SettingsComponent.updateBooleanOption(document.getElementById('syncZoom').classList, AppComponent.synchronousZoom);
    SettingsComponent.updateBooleanOption(document.getElementById('syncScroll').classList, AppComponent.synchronousScroll);
    SettingsComponent.updateBooleanOption(document.getElementById('syncCollapse').classList, AppComponent.synchronousCollapse);
  }

  private static resetContentOptionsMenu(): void {
    let contentOptionMenuChildren: HTMLCollection = document.getElementById('contentOptionMenu').children;
    SettingsComponent.removeIElements(contentOptionMenuChildren);
  }

  private static removeIElements(children: HTMLCollection): void {
    for (let i = 0; i < children.length; i++) {
      let child: HTMLElement = <HTMLElement>children[i].children[0];
      DomUtils.removeIElementChildren(child);
    }
  }

  static showFirstCodeMirror() {
    return AppComponent.viewOptions.codeAndAstDiff || AppComponent.viewOptions.codeOnly || AppComponent.viewOptions.codeDiff;
  }

  static showSecondCodeMirror() {
    return AppComponent.viewOptions.codeAndAstDiff || AppComponent.viewOptions.codeDiff;
  }


  static showFirstTree() {
    return AppComponent.viewOptions.astOnly || AppComponent.viewOptions.astDiff || AppComponent.viewOptions.codeAndAstDiff;
  }

  static showSecondTree() {
    return AppComponent.viewOptions.codeAndAstDiff || AppComponent.viewOptions.astDiff;
  }

  setContentComponent(contentComponent: ContentComponent) {
    this.contentComponent = contentComponent;
  }

  static onlyCode() {
    return AppComponent.viewOptions.codeOnly || AppComponent.viewOptions.codeDiff;
  }

  static onlyAst() {
    return AppComponent.viewOptions.astOnly || AppComponent.viewOptions.astDiff;
  }

  static showDiff(): boolean {
    return SettingsComponent.showSecondTree() || SettingsComponent.showSecondCodeMirror();
  }

  public static syncZoomEnabled(): boolean {
    return document.getElementById('syncZoom').classList.contains('enabled');
  }

  public static syncCollapseEnabled(): boolean {
    return document.getElementById('syncCollapse').classList.contains('enabled');
  }

  public static syncScrollEnabled(): boolean {
    return document.getElementById('syncScroll').classList.contains('enabled');
  }

  btnManageFileVersionsClick() {
    this.router.navigate(['filemanager']);
  }

}
