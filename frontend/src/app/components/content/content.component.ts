import {AfterViewInit, Component, ComponentFactoryResolver, HostListener, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {CodemirrorComponent} from '../codemirror/codemirror.component';
import {D3treeComponent} from '../d3tree/d3tree.component';
import {SettingsComponent} from '../settings/settings.component';
import * as $ from 'jquery';
import {Constants} from '../../utils/Constants';
import {FilechooserComponent} from '../filechooser/filechooser.component';
import {Tree} from '../../models/Tree';
import {TreeComparator} from '../../service/TreeComparator';
import {AppComponent} from '../../app.component';
// import {ITree} from '../../models/ITree';
import {NofileComponent} from '../nofile/nofile.component';
import {AstNode} from '../../models/AstNode';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit, AfterViewInit {
  @ViewChild('treeParent', {read: ViewContainerRef}) private treeContainer: ViewContainerRef;
  @ViewChild('codeMirrorParent', {read: ViewContainerRef}) private codeMirrorContainer: ViewContainerRef;
  @ViewChild('filechooserComponent') private filechooserComponent: FilechooserComponent;

  private leftTree: D3treeComponent;
  private rightTree: D3treeComponent;
  private leftCodeMirror: CodemirrorComponent;
  private rightCodeMirror: CodemirrorComponent;

  private orientation: string = 'horizontal';
  private codeMirrorHeightPercentage: number = 30;   // code mirror height in percent
  private leftTreeData: Tree;
  private rightTreeData: Tree;

  constructor(
    private cfr: ComponentFactoryResolver
  ) {
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.filechooserComponent.setContentComponent(this);
    this.init();
  }

  init() {
    this.filechooserComponent.updateDiff(SettingsComponent.showDiff());
    this.codeMirrorContainer.clear();
    this.treeContainer.clear();
    D3treeComponent.resetIdCounter();
    SettingsComponent.reinitializeSettings();

    this.initCodeMirrorComponents();
    this.initTreeComponents();
    this.setComponentSize();

    if (AppComponent.selectedFile1 == null || AppComponent.selectedFile2 == null) {
      let comp = this.cfr.resolveComponentFactory(NofileComponent);
      this.treeContainer.createComponent(comp);
    } else {
      if (SettingsComponent.showFirstTree() && !SettingsComponent.showSecondTree()) {
        let treeOne: AstNode;
        if (AppComponent.selectedFile1 != null) {
          treeOne = AppComponent.selectedFile1.tree;
        }

        if (treeOne != null) {
          this.leftTreeData = new Tree(treeOne);
        }
      } else {
        let treeOne: AstNode;
        let treeTwo: AstNode;
        if (AppComponent.selectedFile1 != null && AppComponent.selectedFile2 != null) {
          treeOne = AppComponent.selectedFile1.tree;
          treeTwo = AppComponent.selectedFile2.tree;
        }

        if (treeOne != null && treeTwo != null) {
          this.leftTreeData = new Tree(treeOne);
          this.rightTreeData = new Tree(treeTwo);
          let comparator: TreeComparator = new TreeComparator();
          comparator.calcDiff(this.leftTreeData, this.rightTreeData);
        }
      }

      this.initCodeMirror();
      this.initTrees();
    }
  }

  private initTrees(): void {
    if (SettingsComponent.showFirstTree() && !SettingsComponent.showSecondTree()) {
      this.leftTree.initTree(this, this.leftTreeData.getAsITreeObject(), this.orientation);
    } else {
      this.leftTree.initTree(this, this.leftTreeData.getAsITreeObject(), this.orientation);
      this.rightTree.initTree(this, this.rightTreeData.getAsITreeObject(), this.orientation);
    }
  }

  private initCodeMirror(): void {
    if (SettingsComponent.showFirstCodeMirror()) {
      this.leftCodeMirror.initCodeMirror(this, AppComponent.selectedFile1.content);
      if (SettingsComponent.showSecondCodeMirror()) {
        this.rightCodeMirror.initCodeMirror(this, AppComponent.selectedFile2.content);
        this.setDiff();
      }
    }
  }

  public updateNode(hashcode: number, caller: D3treeComponent) { //
    if (this.leftTree === caller) {
      this.rightTree.updateNode(hashcode);
    } else {
      this.leftTree.updateNode(hashcode);
    }
  }

  public changeTreeOrientation(orientation: string) {
    this.orientation = orientation;
    this.initTrees();
  }

  private initTreeComponents() {
    if (SettingsComponent.showFirstTree()) {
      let comp = this.cfr.resolveComponentFactory(D3treeComponent);
      // Create component inside container
      let d3TreeComponent = this.treeContainer.createComponent(comp);
      // see explanations
      this.leftTree = <D3treeComponent>d3TreeComponent.instance;
      this.leftTree.setElementRef(d3TreeComponent);
      this.leftTree.setClass(SettingsComponent.showSecondTree() ? 'col-sm-6' : 'col-sm-12');

      if (SettingsComponent.showSecondTree()) {
        d3TreeComponent = this.treeContainer.createComponent(comp);
        // see explanations
        this.rightTree = <D3treeComponent>d3TreeComponent.instance;
        this.rightTree.setElementRef(d3TreeComponent);
        this.rightTree.setClass('col-sm-6');
      }
    }
  }

  private initCodeMirrorComponents() {
    if (SettingsComponent.showFirstCodeMirror()) {
      let comp = this.cfr.resolveComponentFactory(CodemirrorComponent);
      // Create component inside container
      let codeMirrorComponent = this.codeMirrorContainer.createComponent(comp);
      // see explanations
      this.leftCodeMirror = <CodemirrorComponent>codeMirrorComponent.instance;
      this.leftCodeMirror.setElementRef(codeMirrorComponent);
      this.leftCodeMirror.setClass(SettingsComponent.showSecondCodeMirror() ? 'col-sm-6' : 'col-sm-12');

      if (SettingsComponent.showSecondCodeMirror()) {
        codeMirrorComponent = this.codeMirrorContainer.createComponent(comp);
        // see explanations
        this.rightCodeMirror = <CodemirrorComponent>codeMirrorComponent.instance;
        this.rightCodeMirror.setElementRef(codeMirrorComponent);
        this.rightCodeMirror.setClass('col-sm-6');
      }
    }
  }

  private setComponentSize(): void {
    let headerHeight = $('.main-header').first().outerHeight(true);
    let footerHeight = $('.main-footer').first().outerHeight(true);
    let content = $('.content').first();
    let contentPadding = content.innerHeight() - content.first().height();
    let filesBarHeight = $('#files-chooser').outerHeight(true);
    let fullHeight = document.body.offsetHeight - (headerHeight + footerHeight +
      contentPadding + filesBarHeight + Constants.SVG_MARGIN + 5); // don't know where to find these 5 last pixels...

    if (SettingsComponent.onlyCode()) {
      this.leftCodeMirror.setPreferredHeight(fullHeight);
      if (SettingsComponent.showDiff()) {
        this.rightCodeMirror.setPreferredHeight(fullHeight);
      }
    } else if (SettingsComponent.onlyAst()) {
      this.leftTree.setPreferredHeight(fullHeight);
      if (SettingsComponent.showDiff()) {
        this.rightTree.setPreferredHeight(fullHeight);
      }
    } else { // code an ast
      this.leftCodeMirror.setPreferredHeight(fullHeight * this.codeMirrorHeightPercentage / 100);
      this.rightCodeMirror.setPreferredHeight(fullHeight * this.codeMirrorHeightPercentage / 100);
      this.leftTree.setPreferredHeight(fullHeight * (100 - this.codeMirrorHeightPercentage) / 100);
      this.rightTree.setPreferredHeight(fullHeight * (100 - this.codeMirrorHeightPercentage) / 100);

    }
  }

  updateScroll(scrollInfo: any, caller: CodemirrorComponent) {
    if (SettingsComponent.syncScrollEnabled() && SettingsComponent.showDiff()) {
      if (this.leftCodeMirror === caller) {
        this.rightCodeMirror.scrollTo(scrollInfo);
      } else {
        this.leftCodeMirror.scrollTo(scrollInfo);
      }
    }
  }

  private setDiff() {
    this.leftTreeData.markDiff(this.leftCodeMirror);
    this.rightTreeData.markDiff(this.rightCodeMirror);
  }

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (event.key == 'e') {
      if (this.leftTree != null && this.rightTree != null) {
        this.leftTree.expandAll();
        this.rightTree.expandAll();
      } else if (this.leftTree != null) {
        this.leftTree.expandAll();
      }
    } else if (event.key == 'c') {
      if (this.leftTree != null && this.rightTree != null) {
        this.leftTree.collapseAll();
        this.rightTree.collapseAll();
      } else if (this.leftTree != null) {
        this.leftTree.collapseAll();
      }
    }
  }

}
