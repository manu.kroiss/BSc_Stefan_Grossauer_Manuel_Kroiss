import {Component, ElementRef, OnInit} from '@angular/core';
import * as CodeMirror from 'codemirror';
import {ContentComponent} from '../content/content.component';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-codemirror',
  templateUrl: './codemirror.component.html',
  styleUrls: ['./codemirror.component.css']
})
export class CodemirrorComponent implements OnInit {

  private cm;
  private contentComponent: ContentComponent;
  private readonly htmlDomElement: HTMLElement;
  private elementRef: CodemirrorComponent;
  private preferredHeight: number;
  private userScrolled: boolean = true;

  constructor(
    private el: ElementRef
  ) {
    this.htmlDomElement = el.nativeElement;
  }

  ngOnInit() {
    if (AppComponent.selectedProgrammingLanguage.filename.toLowerCase() == 'clike') {
      import('codemirror/mode/clike/clike');
    } else if (AppComponent.selectedProgrammingLanguage.filename.toLowerCase() == 'javascript') {
      import('codemirror/mode/javascript/javascript');
    }
  }


  public setPreferredHeight(height: number) {
    this.preferredHeight = height;
  }

  public markDiff(from: number, to: number, fromChar: number, toChar: number) {
    this.cm.markText({line: from, ch: fromChar}, {line: to, ch: toChar + 1}, {className: 'myBg'});
  }

  initCodeMirror(contentComponent: ContentComponent, code: string) {
    this.contentComponent = contentComponent;

    this.cm = CodeMirror(this.htmlDomElement, {
      lineNumbers: true,
      readOnly: true,
      mode: AppComponent.selectedProgrammingLanguage.mimeType || 'text/plain'
    });
    this.cm.setSize(null, this.preferredHeight != null ? this.preferredHeight : '100%');

    this.cm.replaceRange(code, {
      line: Infinity
    });

    this.cm.setSelection({
      'line': this.cm.firstLine(),
      'ch': 0,
      'sticky': null
    }, {
      'line': this.cm.firstLine(),
      'ch': 0,
      'sticky': null
    }, {
      scroll: true
    });

    this.cm.on('scroll', (instance: CodeMirror) => this.onScroll(instance), {passive: true});

  }

  private onScroll(instance: CodeMirror) {
    if (this.userScrolled) {
      this.contentComponent.updateScroll(instance.getScrollInfo(), this);
    } else {
      this.userScrolled = true;
    }
  }

  public setClass(className: string): void {
    this.htmlDomElement.setAttribute('class', className);
  }

  public setElementRef(elementRef: any): void {
    this.elementRef = elementRef;
  }

  public scrollTo(scrollInfo: any) {
    this.userScrolled = false;
    this.cm.scrollTo(scrollInfo.left, scrollInfo.top);
  }
}
