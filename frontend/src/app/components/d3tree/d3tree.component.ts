import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import * as d3 from 'd3';
import {TreeLayout} from 'd3';
import {HttpClient} from '@angular/common/http';
import {ContentComponent} from '../content/content.component';
import {Constants} from '../../utils/Constants';
import {SettingsComponent} from '../settings/settings.component';

@Component({
  selector: 'app-d3tree',
  templateUrl: './d3tree.component.html',
  styleUrls: ['./d3tree.component.css']
})

export class D3treeComponent implements OnInit, AfterViewInit {
  private static id: number = 1;
  private contentComponent: ContentComponent;

  private componentId: number;
  private timeout: number = 500;

  private nodeSize: number = 10;
  private zero: number = 1e-6;
  private codemirrorPadding: number = 30;
  private margin = {
    top: 20,
    right: 120,
    bottom: 20,
    left: 120
  };
  private levelGap: number;
  private horizontaltextOffset: number;
  private verticalTextOffset: number;

  private duration: number = 750;
  private treeLayout: TreeLayout<any>;
  private root;
  private svg;
  private orientation: String;
  private i = 0; // ids for the nodes

  private headerHeight: number = 50;
  private elementRef: D3treeComponent; // object element
  private htmlDomElement: HTMLElement;
  private preferredHeight: number;

  public setElementRef(elementRef: any) {
    this.elementRef = elementRef;
  }

  constructor(
    private http: HttpClient,
    private el: ElementRef
  ) {
    this.htmlDomElement = this.el.nativeElement;
  }

  ngOnInit() {
    this.componentId = D3treeComponent.id;
    D3treeComponent.id++;
  }

  static resetIdCounter(): void {
    D3treeComponent.id = 1;
  }

  ngAfterViewInit() {
  }

  reset() {
    while (this.htmlDomElement.firstChild) {
      this.htmlDomElement.removeChild(this.htmlDomElement.firstChild);
    }
  }

  public initTree(contentComponent: ContentComponent, data, orientation) {

    this.contentComponent = contentComponent;
    this.reset();
    this.orientation = orientation;

    // ************** Generate the tree diagram	 *****************
    setTimeout(() => { // timeout because there is no actual solution for recognizing when page reload has finished
      this.createTree(data);
    }, this.timeout);
  }

  private collapse(d) {
    if (d.children) {
      d._children = d.children;
      d._children.forEach(elem => {
        this.collapse(elem);
      });
      d.children = null;
    }
  }

  private expand(d) {
    if (d._children) {
      d.children = d._children;
      d.children.forEach(elem => {
        this.expand(elem);
      });
      d._children = null;
    }
  }

  public expandAll() {
    this.root.children.forEach(elem => {
      this.expand(elem);
    });
    this.update(this.root);
  }

  public collapseAll() {
    this.root.children.forEach(elem => {
      this.collapse(elem);
    });
    this.update(this.root);
  }

  private createTree(data) {
    this.levelGap = this.orientation == 'vertical' ? 130 : 150;
    this.horizontaltextOffset = this.orientation == 'vertical' ? 18 : 13;
    this.verticalTextOffset = this.orientation == 'vertical' ? 18 : 18;
    let clientWidth = this.htmlDomElement.getBoundingClientRect().width;

    let width = clientWidth - this.margin.right - this.margin.left - this.codemirrorPadding;
    let height = (this.preferredHeight != null ? this.preferredHeight : document.body.offsetHeight / 2 - this.headerHeight) - this.margin.top - this.margin.bottom;

    this.treeLayout = this.orientation == 'vertical' ? d3.tree().size([width, height]) : d3.tree().size([height, width]);

    this.svg = d3.select(this.htmlDomElement).append('svg')
      .attr('style', 'margin-top: ' + Constants.SVG_MARGIN + 'px')
      .attr('class', 'border svg' + this.componentId)
      .attr('width', Math.max(width + this.margin.right + this.margin.left, 0))
      .attr('height', height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('class', 'drawarea' + this.componentId)
      .append('g')
      .attr('transform', 'translate(' + (this.margin.left) + ',' + this.margin.top + ')');


    this.root = d3.hierarchy(data, (d) => {
      return d.children;
    });
    this.root.x0 = this.orientation == 'vertical' ? 0 : height / 2;
    this.root.y0 = this.orientation == 'vertical' ? width / 2 : 0;

    this.root.children.forEach(elem => {
      this.collapse(elem);
    });
    this.update(this.root);

    //Add zoom extension
    d3.select('.svg' + this.componentId).call(d3.zoom()
      .scaleExtent([0.5, 5])
      .on('zoom', () => this.zoom(width, height)));
  }

  private zoom(width, height) {
    let transform = d3.event.transform;

    let scale = transform.k;
    let scaleFactor = 2;
    let drawAreaWidth = document.getElementsByClassName('drawarea' + this.componentId).item(0).getBoundingClientRect().width;
    let tBound = this.orientation == 'vertical' ? -height * scale + scaleFactor * scale : -height * scale + scaleFactor * scale; // top
    let bBound = height - scaleFactor * scale; // bottom
    let lBound = -drawAreaWidth * scale + scaleFactor * scale; // left
    let rBound = width - scaleFactor * scale; // right

    transform.x = Math.max(Math.min(transform.x, rBound), lBound);
    transform.y = Math.max(Math.min(transform.y, bBound), tBound);

    d3.select('.drawarea' + this.componentId)
      .attr('transform', transform);

    if (SettingsComponent.syncZoomEnabled()) {
      if ('.drawarea' + this.componentId === '.drawarea1') {
        d3.select('.drawarea2')
          .attr('transform', transform);
      } else {
        d3.select('.drawarea1')
          .attr('transform', transform);
      }
    }
  }

  // Creates a curved (diagonal) path from parent to the child nodes
  private diagonal(s, d) {
    if (this.orientation == 'vertical') {
      return `M ${s.x} ${s.y}
          C ${(s.x + d.x) / 2} ${s.y},
            ${(s.x + d.x) / 2} ${d.y},
            ${d.x} ${d.y}`;
    } else {
      return `M ${s.y} ${s.x}
              C ${(s.y + d.y) / 2} ${s.x},
                ${(s.y + d.y) / 2} ${d.x},
                ${d.y} ${d.x}`;
    }
  }

  private appendText(nodeEnter: any, attribute: string, line: number) {
    let text = nodeEnter.append('text')
      .attr('x', (d: any) => { // horizontal offset
        if (this.orientation == 'vertical') {
          return 0;
        } else {
          // check whether node has children or not
          return d.children || d._children ? -this.horizontaltextOffset : this.horizontaltextOffset;
        }
      })
      .attr('y', (d: any) => { // vertical offset
        if (this.orientation == 'vertical') {
          return line * this.verticalTextOffset;
        } else {
          return d.children || d._children
            ? (d.depth % 2 == 0 ? -(2 - line) * this.verticalTextOffset : line * this.verticalTextOffset + this.verticalTextOffset)
            : line * this.verticalTextOffset;
        }
      })
      .attr('dy', '.35em')
      .attr('text-anchor', (d: any) => {
        if (this.orientation == 'vertical') {
          return 'end';
        } else {
          return d.children || d._children ? 'end' : 'start';
        }
      })
      .style('fill-opacity', this.zero);

    if (attribute == 'name') {
      text.text((d: any) => {
        return d.data.name;
      })
        .attr('visibility', (d: any) => {
          return d.data.name == null ? 'hidden' : 'visible';
        });
    } else if (attribute == 'type') {
      text.text((d: any) => {
        return '<<' + d.data.type + '>>' + (d.data.operator == null ? '' : ' : ' + d.data.operator) + (d.data.value == null ? '' : ': ' + d.data.value);
      })
        .attr('visibility', (d: any) => {
          return d.data.type == null ? 'hidden' : 'visible';
        });
    }
  }

  private update(source) {
    // Compute the new tree layout.
    let treeData = this.treeLayout(this.root);
    let nodes = treeData.descendants();
    let links = treeData.descendants().slice(1);
    // Normalize for fixed-depth.
    nodes.forEach(d => {
      d.y = d.depth * this.levelGap;
    });

    // Update the nodes…
    let node = this.svg.selectAll('g.node')
      .data(nodes, (d: any) => {
        return d.id || (d.id = ++this.i);
      });

    // Enter any new nodes at the parent's previous position.
    let nodeEnter = node.enter().append('g')
      .attr('class', 'node cursor')
      .attr('transform', () => {
        if (this.orientation == 'vertical') {
          return 'translate(' + source.x0 + ',' + source.y0 + ')';
        } else {
          return 'translate(' + source.y0 + ',' + source.x0 + ')';
        }
      })
      .on('click', d => this.click(d));

    nodeEnter.append('circle')
      .attr('class', 'node')
      .attr('r', this.zero)
      .style('fill', (d: any) => D3treeComponent.styleNode(d));

    this.appendText(nodeEnter, 'type', 0);
    this.appendText(nodeEnter, 'name', 1);

    if (this.orientation == 'vertical') {
      d3.selectAll('g text').attr('transform', 'translate(-10,15) rotate(-90)');
    }

    // Transition nodes to their new position.
    let nodeUpdate = nodeEnter.merge(node);

    nodeUpdate.transition()
      .duration(this.duration)
      .attr('transform', (d) => {
        if (this.orientation == 'vertical') {
          return 'translate(' + d.x + ',' + d.y + ')';
        } else {
          return 'translate(' + d.y + ',' + d.x + ')';
        }
      });

    nodeUpdate.select('circle.node')
      .attr('r', this.nodeSize)
      .style('fill', (d: any) => D3treeComponent.styleNode(d));

    nodeUpdate.selectAll('g text')
      .style('fill-opacity', 1);

    // Transition exiting nodes to the parent's new position.
    let nodeExit = node.exit().transition()
      .duration(this.duration)
      .attr('transform', () => {
        if (this.orientation == 'vertical') {
          return 'translate(' + source.x + ',' + source.y + ')';
        } else {
          return 'translate(' + source.y + ',' + source.x + ')';
        }
      })
      .remove();

    nodeExit.select('circle')
      .attr('r', this.zero);

    nodeExit.select('text')
      .style('fill-opacity', this.zero);

    // Update the links…
    let link = this.svg.selectAll('path.link')
      .data(links, (d: any) => {
        return d.id;
      });

    // Enter any new links at the parent's previous position.
    let linkEnter = link.enter().insert('path', 'g')
      .attr('class', 'link')
      .attr('d', () => {
        if (this.orientation == 'vertical') {
          let o = {y: source.y0, x: source.x0};
          return this.diagonal(o, o);
        } else {
          let o = {x: source.x0, y: source.y0};
          return this.diagonal(o, o);
        }
      });

    // Transition links to their new position.
    let linkUpdate = linkEnter.merge(link);
    linkUpdate.transition()
      .duration(this.duration)
      .attr('d', d => {
        if (this.orientation == 'vertical') {
          return this.diagonal(d, d.parent);
        } else {
          return this.diagonal(d.parent, d);
        }
      });

    // Transition exiting links to the parent's new position.
    link.exit().transition()
      .duration(this.duration)
      .attr('d', () => {
        let o = {x: source.x, y: source.y};
        return this.diagonal(o, o);
      })
      .remove();

    // Stash the old positions for transition.
    nodes.forEach((d: any) => {
      d.x0 = d.x;
      d.y0 = d.y;
    });
  }

  private static styleNode(d: any) {
    if (d.data.diff) {
      return d._children ? '#fc6b5d' : '#ffb5a8';
    } else {
      return d._children ? 'lightsteelblue' : '#ffffff';
    }
  }

  private click(d: any) {
    this.collapseExpand(d);
    if (SettingsComponent.syncCollapseEnabled()) {
      this.contentComponent.updateNode(d.data.hashcode, this);
    }
  }

  private collapseExpand(d: any) {
    if (d.children) {
      d._children = d.children;
      d.children = null;
    } else {
      d.children = d._children;
      d._children = null;
    }
    this.update(d);
  }

  public updateNode(hashcode: number) {
    let syncNode = this.getSyncNode(hashcode, this.root);

    if (syncNode != null) {
      this.collapseExpand(syncNode);
    }
  }

  private getSyncNode(hashcode: number, node: any): any {
    if (node == null) return;
    if (node.data.hashcode === hashcode) {
      return node;
    }
    if (node.children != null) {
      for (let i = 0; i < node.children.length; i++) {
        let syncNode = this.getSyncNode(hashcode, node.children[i]);
        if (syncNode != null) return syncNode;
      }
    }
    if (node._children != null) {
      for (let i = 0; i < node._children.length; i++) {
        let syncNode = this.getSyncNode(hashcode, node._children[i]);
        if (syncNode != null) return syncNode;
      }
    }
    return null;
  }

  public destroy(): void {
    this.elementRef.destroy();
  }

  public setClass(className: string): void {
    this.htmlDomElement.setAttribute('class', className);
  }

  setPreferredHeight(height: number) {
    this.preferredHeight = height;
  }
}
