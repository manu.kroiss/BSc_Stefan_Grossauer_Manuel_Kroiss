import {Component, OnInit} from '@angular/core';
import {SourceFile} from '../../models/SourceFile';
import {AppComponent} from '../../app.component';
import {ContentComponent} from '../content/content.component';

@Component({
  selector: 'app-filechooser',
  templateUrl: './filechooser.component.html',
  styleUrls: ['./filechooser.component.css']
})
export class FilechooserComponent implements OnInit {
  private contentComponent: ContentComponent;

  public sourceFiles: Array<SourceFile>;
  public selectedFile1: SourceFile;
  public selectedFile2: SourceFile;
  diff: boolean = true;

  constructor() {
    this.sourceFiles = AppComponent.getSourceFiles();
    if (this.sourceFiles.length > 0) {
      this.selectedFile1 = this.sourceFiles[0];
      this.selectedFile2 = this.sourceFiles[0];
      AppComponent.selectedFile1 = this.sourceFiles[0];
      AppComponent.selectedFile2 = this.sourceFiles[0];
    }
  }

  ngOnInit() {

  }

  public setContentComponent(contentComponent: ContentComponent) {
    this.contentComponent = contentComponent;
  }

  onSelectChange1(newFile: SourceFile) {
    this.selectedFile1 = newFile;
    AppComponent.selectedFile1 = newFile;
    this.contentComponent.init();
  }

  onSelectChange2(newFile: SourceFile) {
    this.selectedFile2 = newFile;
    AppComponent.selectedFile2 = newFile;
    this.contentComponent.init();
  }

  public updateDiff(diff: boolean): void {
    this.diff = diff;
  }

}
