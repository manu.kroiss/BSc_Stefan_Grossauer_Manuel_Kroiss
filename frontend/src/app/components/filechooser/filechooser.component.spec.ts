import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilechooserComponent} from './filechooser.component';

describe('FilechooserComponent', () => {
  let component: FilechooserComponent;
  let fixture: ComponentFixture<FilechooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilechooserComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilechooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
