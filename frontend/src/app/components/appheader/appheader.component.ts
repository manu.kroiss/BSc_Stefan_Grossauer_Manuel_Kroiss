import {Component, OnInit} from '@angular/core';
import {ContentComponent} from '../content/content.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {
  private contentComponent: ContentComponent;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  sidebarToggled() {
    this.contentComponent.init();
  }

  setContentComponent(contentComponent: ContentComponent) {
    this.contentComponent = contentComponent;
  }

  logoClick() {
    this.router.navigate(['index']);
  }
}
