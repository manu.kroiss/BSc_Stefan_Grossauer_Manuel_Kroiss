import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilemanagercontentComponent} from './filemanagercontent.component';

describe('FilemanagercontentComponent', () => {
  let component: FilemanagercontentComponent;
  let fixture: ComponentFixture<FilemanagercontentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilemanagercontentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilemanagercontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
