import {AfterViewInit, Component, OnInit} from '@angular/core';
import {SourceFile} from '../../models/SourceFile';
import {Router} from '@angular/router';
import {AppComponent} from '../../app.component';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http';
// import {ITree} from '../../models/ITree';
import {AstService} from '../../service/restService/ast.service';
import {AstNode} from '../../models/AstNode';

@Component({
  selector: 'app-filemanagercontent',
  templateUrl: './filemanagercontent.component.html',
  styleUrls: ['./filemanagercontent.component.css']
})
export class FilemanagercontentComponent implements OnInit, AfterViewInit {
  sourceFiles: Array<SourceFile>;
  private tmpFile: File;
  private tmpContent: string;
  private tmpTree: AstNode;
  inputError: boolean = false;
  idExists: boolean = false;
  languageError: boolean = false;
  unknownError: boolean = false;

  constructor(
    private router: Router,
    private http: HttpClient,
    private astService: AstService
  ) {
  }

  ngOnInit() {
    this.sourceFiles = AppComponent.getSourceFiles();
  }

  ngAfterViewInit() {
    FilemanagercontentComponent.addInputLabelChangeListener();
  }

  private static addInputLabelChangeListener() {
    let inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
      let label = input.nextElementSibling,
        labelVal = label.innerHTML;
      input.addEventListener('change', function (event) {
        let fileName = '';
        if (this.files && this.files.length > 1)
          fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
        else
          fileName = event.target.value.split('\\').pop();

        if (fileName)
          label.innerHTML = fileName;
        else
          label.innerHTML = labelVal;
      });
    });
  }

  btnDeleteFile(index: number) {
    let deleted = this.sourceFiles.splice(index, 1);
    if (AppComponent.selectedFile1 === deleted[0]) {
      AppComponent.selectedFile1 = null;
    }
    if (AppComponent.selectedFile2 === deleted[0]) {
      AppComponent.selectedFile2 = null;
    }
  }

  btnOkClick() {
    this.router.navigate(['index']);
  }

  onFileChange(event: any) {
    let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    let files: FileList = target.files;
    this.tmpFile = files[0];
    this.languageError = false;
    this.unknownError = false;

    this.astService.ast(AppComponent.selectedProgrammingLanguage.name.toLowerCase(), files[0])
      .subscribe(data => {
        this.tmpTree = data;
      }, error => {
        if (error.status == 501) {
          this.languageError = true;
        } else {
          this.unknownError = true;
        }
      });

    if (!this.languageError) {
      let reader: FileReader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        let file = event.target.files[0];
        reader.readAsText(file, 'UTF-8');
        reader.onload = () => {
          this.tmpContent = reader.result;
        };
      }
    }
  }

  onSubmit(form: HTMLFormElement): void {
    if (!form || !form.value || !form.value['identifier'] || this.tmpFile == null) {
      this.inputError = true;
      return;
    } else {
      let exist: boolean = false;
      for (let i = 0; i < this.sourceFiles.length; i++) {
        if (this.sourceFiles[i].identifier == form.value['identifier']) {
          this.idExists = true;
          exist = true;
          break;
        }
      }
      if (!exist) this.idExists = false;
      if (!this.idExists && !this.languageError && !this.unknownError) {
        this.sourceFiles.push(new SourceFile(this.tmpFile.name, form.value['identifier'], new Date(this.tmpFile.lastModifiedDate), this.tmpContent, this.tmpTree));
        this.closeModalAndReset(form);
      }
    }
  }

  closeModalAndReset(form: HTMLFormElement) {
    let btnClose: HTMLElement = document.getElementById('btnCloseModal');
    btnClose.click();
    form.reset();
    this.tmpFile = null;
    $('#lbl-file-upload').html('Select file');
    this.inputError = false;
    this.idExists = false;
  }
}
