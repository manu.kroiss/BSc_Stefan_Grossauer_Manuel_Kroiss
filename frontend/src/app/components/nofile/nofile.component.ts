import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nofile',
  templateUrl: './nofile.component.html',
  styleUrls: ['./nofile.component.css']
})
export class NofileComponent implements OnInit {

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  btnUploadFileClick() {
    this.router.navigate(['filemanager']);
  }
}
