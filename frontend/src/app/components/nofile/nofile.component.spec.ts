import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NofileComponent} from './nofile.component';

describe('NofileComponent', () => {
  let component: NofileComponent;
  let fixture: ComponentFixture<NofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NofileComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
