import {AfterViewInit, Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {SettingsComponent} from '../settings/settings.component';
import {ContentComponent} from '../content/content.component';
import {AppheaderComponent} from '../appheader/appheader.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, AfterViewInit {

  @ViewChild('headerComponent') private headerComponent: AppheaderComponent;
  @ViewChild('contentComponent') private contentComponent: ContentComponent;
  @ViewChild('settingsComponent') private settingsComponent: SettingsComponent;


  @ViewChild('contentParent', {read: ViewContainerRef}) private contentComponentContainer: ViewContainerRef;


  constructor() {

  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.headerComponent.setContentComponent(this.contentComponent);
    this.settingsComponent.setContentComponent(this.contentComponent);
  }


}
