import {Component} from '@angular/core';
import {SourceFile} from './models/SourceFile';
import {Language} from './models/Language';
import {ISupportedLanguages} from './models/ISupportedLanguages';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  private static sourceFiles: Array<SourceFile> = [];
  // private static _selectedProgrammingLanguage: Language;

  private static _selectedFile1: SourceFile = null;
  private static _selectedFile2: SourceFile = null;

  private static _synchronousZoom: boolean = true;
  private static _synchronousCollapse: boolean = true;
  private static _synchronousScroll: boolean = true;
  private static _horizontalTree: boolean = true;
  private static _supportedLanguages: Array<Language> = [];
  private static _viewOptions = {
    codeAndAstDiff: true,
    codeDiff: false,
    astDiff: false,
    codeOnly: false,
    astOnly: false
  };

  constructor(
    private http: HttpClient
  ) {
    this.http.get('./assets/res/supported_languages.json').subscribe(res => {
      let tmp: ISupportedLanguages = <ISupportedLanguages>res;
      tmp.supportedLanguages.forEach(lan => {
        AppComponent._supportedLanguages.push(new Language(lan.name, false, lan.filename, lan.mimeType));
      });
      AppComponent._supportedLanguages[0].selected = true;
    }, error => {
      throw error;
    });
  }

  public static getSourceFiles(): Array<SourceFile> {
    return this.sourceFiles;
  }
  static get selectedProgrammingLanguage(): Language {
    for (let i = 0; i < this._supportedLanguages.length; i++) {
      if (this._supportedLanguages[i].selected) return this._supportedLanguages[i];
    }
    return new Language('java', true, 'clike', 'text/x-java');
  }

  static get selectedFile1(): SourceFile {
    return this._selectedFile1;
  }

  static set selectedFile1(value: SourceFile) {
    this._selectedFile1 = value;
  }

  static get selectedFile2(): SourceFile {
    return this._selectedFile2;
  }

  static set selectedFile2(value: SourceFile) {
    this._selectedFile2 = value;
  }

  static get synchronousZoom(): boolean {
    return this._synchronousZoom;
  }

  static set synchronousZoom(value: boolean) {
    this._synchronousZoom = value;
  }

  static get synchronousCollapse(): boolean {
    return this._synchronousCollapse;
  }

  static set synchronousCollapse(value: boolean) {
    this._synchronousCollapse = value;
  }

  static get synchronousScroll(): boolean {
    return this._synchronousScroll;
  }

  static set synchronousScroll(value: boolean) {
    this._synchronousScroll = value;
  }


  static get horizontalTree(): boolean {
    return this._horizontalTree;
  }

  static set horizontalTree(value: boolean) {
    this._horizontalTree = value;
  }

  static get supportedLanguages(): Array<Language> {
    return this._supportedLanguages;
  }

  static set supportedLanguages(value: Array<Language>) {
    this._supportedLanguages = value;
  }

  static get viewOptions(): { codeAndAstDiff: boolean; codeDiff: boolean; astDiff: boolean; codeOnly: boolean; astOnly: boolean } {
    return this._viewOptions;
  }

  static set viewOptions(value: { codeAndAstDiff: boolean; codeDiff: boolean; astDiff: boolean; codeOnly: boolean; astOnly: boolean }) {
    this._viewOptions = value;
  }
}
