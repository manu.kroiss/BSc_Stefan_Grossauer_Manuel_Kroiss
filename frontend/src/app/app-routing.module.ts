import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FilemanagerComponent} from './components/filemanager/filemanager.component';
import {IndexComponent} from './components/index/index.component';

const routes: Routes = [
  {path: '', redirectTo: '/index', pathMatch: 'full'},
  {path: 'index', component: IndexComponent},
  {path: 'filemanager', component: FilemanagerComponent},
  {path: '**', redirectTo: '/index', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
