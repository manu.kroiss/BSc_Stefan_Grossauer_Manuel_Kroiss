/**
 * Abstract Syntax Tree Generation
 * This is a server for generating Abstract Syntax Trees (ASTs) from a given source code file. You can find out more about it at [https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss](   https://gitlab.com/manu.kroiss/BSc_Stefan_Grossauer_Manuel_Kroiss).
 *
 * OpenAPI spec version: 0.1.0
 * Contact: e1525664@student.tuwien.ac.at
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface AstNode {
  /**
   * The type of the AST node
   */
  type?: AstNode.TypeEnum;
  /**
   * Used for the name of e.g. a function
   */
  name?: string;
  /**
   * The numeric value of a number -> needs casting
   */
  value?: number;
  /**
   * Used for the operator in a binary expression, e.g. '<'.
   */
  operator?: string;
  startLine?: number;
  endLine?: number;
  startChar?: number;
  endChar?: number;
  hashcode?: number;
  diff?: boolean;
  children?: Array<AstNode>;
}

export namespace AstNode {
  export type TypeEnum =
    'root'
    | 'class'
    | 'function_declaration'
    | 'parameters'
    | 'parameter'
    | 'if'
    | 'condition'
    | 'then'
    | 'else'
    | 'expression'
    | 'statement'
    | 'binary_expression'
    | 'variable'
    | 'decimal'
    | 'float'
    | 'return'
    | 'function_call';
  export const TypeEnum = {
    Root: 'root' as TypeEnum,
    Class: 'class' as TypeEnum,
    FunctionDeclaration: 'function_declaration' as TypeEnum,
    Parameters: 'parameters' as TypeEnum,
    Parameter: 'parameter' as TypeEnum,
    If: 'if' as TypeEnum,
    Condition: 'condition' as TypeEnum,
    Then: 'then' as TypeEnum,
    Else: 'else' as TypeEnum,
    Expression: 'expression' as TypeEnum,
    Statement: 'statement' as TypeEnum,
    BinaryExpression: 'binary_expression' as TypeEnum,
    Variable: 'variable' as TypeEnum,
    Decimal: 'decimal' as TypeEnum,
    Float: 'float' as TypeEnum,
    Return: 'return' as TypeEnum,
    FunctionCall: 'function_call' as TypeEnum
  };
}
