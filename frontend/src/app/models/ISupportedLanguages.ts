interface ISupportedLanguage {
  name: string;
  filename: string;
  mimeType: string;
}

export interface ISupportedLanguages {
  supportedLanguages: Array<ISupportedLanguage>;
}
