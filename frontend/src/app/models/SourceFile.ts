import {AstNode} from './AstNode';

export class SourceFile {
  private _filename: string;
  private _identifier: string;
  private _lastModifiedDate: Date;
  private _content: string;
  private _tree: AstNode;

  constructor(filename: string, name: string, uploadTime: Date, content: string, tree: AstNode) {
    this._filename = filename;
    this._identifier = name;
    this._lastModifiedDate = uploadTime;
    this._content = content;
    this._tree = tree;
  }

  get filename(): string {
    return this._filename;
  }

  set filename(value: string) {
    this._filename = value;
  }

  get identifier(): string {
    return this._identifier;
  }

  set identifier(value: string) {
    this._identifier = value;
  }

  get lastModifiedDate(): Date {
    return this._lastModifiedDate;
  }

  set lastModifiedDate(value: Date) {
    this._lastModifiedDate = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }


  get tree(): AstNode {
    return this._tree;
  }

  set tree(value: AstNode) {
    this._tree = value;
  }
}
