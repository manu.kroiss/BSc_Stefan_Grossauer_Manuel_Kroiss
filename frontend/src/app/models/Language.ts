export class Language {
  private _name: string;
  private _selected: boolean;
  private _filename: string;
  private _mimeType: string;


  constructor(name: string, selected: boolean, filename: string, mimeType: string) {
    this._name = name;
    this._selected = selected;
    this._filename = filename;
    this._mimeType = mimeType;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get selected(): boolean {
    return this._selected;
  }

  set selected(value: boolean) {
    this._selected = value;
  }


  get filename(): string {
    return this._filename;
  }

  set filename(value: string) {
    this._filename = value;
  }

  get mimeType(): string {
    return this._mimeType;
  }

  set mimeType(value: string) {
    this._mimeType = value;
  }
}
