import {StringUtil} from '../utils/StringUtil';
import {CodemirrorComponent} from '../components/codemirror/codemirror.component';
import {AstNode} from './AstNode';

export class Tree {
  private root: AstNode;
  private readonly _children: Array<Tree>;
  private _diff: boolean = false;
  private _similarHash: number;
  private _codeMirrorDiff: boolean = false;

  constructor(data: any) {
    this.root = data;
    this._children = [];

    if (this.root.children) {
      this.root.children.forEach(child => {
        this._children[this._children.length] = new Tree(child);
      });
    }
  }

  get codeMirrorDiff(): boolean {
    return this._codeMirrorDiff;
  }

  set codeMirrorDiff(value: boolean) {
    this._codeMirrorDiff = value;
  }

  public getName(): string {
    return this.root.name;
  }

  public getType(): AstNode.TypeEnum {
    return this.root.type;
  }

  public getValue(): number {
    return this.root.value;
  }

  public getOperator(): string {
    return this.root.operator;
  }

  public getStartLine(): number {
    return this.root.startLine;
  }

  public getEndLine(): number {
    return this.root.endLine;
  }

  public getStartChar(): number {
    return this.root.startChar == null ? 0 : this.root.startChar;
  }

  public getEndChar(): number {
    return this.root.endChar == null ? 1 : this.root.endChar;
  }

  get diff(): boolean {
    return this._diff;
  }

  set diff(value: boolean) {
    this._diff = value;
  }

  get children(): Array<Tree> {
    return this._children;
  }

  public hashcode(): number {
    let hash: number = StringUtil.getStringHash(this.getName());
    hash += StringUtil.getStringHash(this.getType());
    hash += this.getValue();
    hash += StringUtil.getStringHash(this.getOperator());
    hash += this._children.length;
    this._children.forEach(child => {
      hash += child.hashcode();
    });
    return hash;
  }

  public parentHashcode(): number {
    let hash: number = StringUtil.getStringHash(this.getName());
    hash += StringUtil.getStringHash(this.getType());
    hash += this.getValue();
    hash += StringUtil.getStringHash(this.getOperator());
    return hash;
  }

  getAsITreeObject(): AstNode {
    return {
      type: this.getType(),
      name: this.getName(),
      value: this.getValue(),
      operator: this.getOperator(),
      startLine: this.getStartLine(),
      endLine: this.getEndLine(),
      startChar: this.getStartChar(),
      endChar: this.getEndChar(),
      hashcode: this._similarHash != null ? this._similarHash : this.hashcode(),
      diff: this._diff,
      children: this.getChildrenAsObject()
    };
  }

  private getChildrenAsObject(): Array<AstNode> {
    let children: Array<AstNode> = [];
    this._children.forEach(child => {
      children.push(child.getAsITreeObject());
    });
    return children;
  }


  set similarHash(value: number) {
    this._similarHash = value;
  }

  markDiff(codeMirror: CodemirrorComponent) {
    if (this._codeMirrorDiff) {
      if (this.root.startLine != null && this.root.endLine != null) {
        codeMirror.markDiff(this.root.startLine - 1, this.root.endLine - 1, this.getStartChar(), this.getEndChar());
      }
    }
    this._children.forEach(child => child.markDiff(codeMirror));
  }
}
