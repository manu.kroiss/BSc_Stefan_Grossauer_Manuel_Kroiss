import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppheaderComponent} from './components/appheader/appheader.component';
import {AppfooterComponent} from './components/appfooter/appfooter.component';
import {SettingsComponent} from './components/settings/settings.component';
import {ContentComponent} from './components/content/content.component';
import {HttpClientModule} from '@angular/common/http';
import {CodemirrorComponent} from './components/codemirror/codemirror.component';
import {D3treeComponent} from './components/d3tree/d3tree.component';
import {FilechooserComponent} from './components/filechooser/filechooser.component';
import {FilemanagerComponent} from './components/filemanager/filemanager.component';
import {AppRoutingModule} from './app-routing.module';
import {IndexComponent} from './components/index/index.component';
import {FilemanagercontentComponent} from './components/filemanagercontent/filemanagercontent.component';
import {NofileComponent} from './components/nofile/nofile.component';
import {AstService} from './service/restService/ast.service';

@NgModule({
  declarations: [
    AppComponent,
    AppheaderComponent,
    AppfooterComponent,
    SettingsComponent,
    ContentComponent,
    CodemirrorComponent,
    D3treeComponent,
    FilechooserComponent,
    FilemanagerComponent,
    IndexComponent,
    FilemanagercontentComponent,
    NofileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    SettingsComponent,
    ContentComponent,
    AstService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    D3treeComponent,
    CodemirrorComponent,
    ContentComponent,
    NofileComponent
  ]
})
export class AppModule { }
